﻿using CoreAnimation;
using CoreGraphics;
using Facebook.LoginKit;
using Foundation;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Json;
using System.Threading.Tasks;
using UIKit;
using Xamarin.InAppPurchase;

namespace Lovie
{
	partial class ProfilePage : UIViewController
	{

        InAppPurchaseManager purchaseManager;
        Dictionary<int,InAppProduct> Prodotti = new Dictionary<int,InAppProduct>();

        int Acquisto = 0;

		public ProfilePage (IntPtr handle) : base (handle)
		{
		}

        public static ProfilePage Instance { get; private set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            AppDelegate.Instance.isHome = false;
            AppDelegate.Instance.isChat = false;
            AppDelegate.Instance.isProfile = true;
            AppDelegate.Instance.isContact = false;

            ProfilePage.Instance = this;

            LogoutItem.Clicked += delegate {

                Logout();

            };

            Nome.Font = UIFont.FromName("Roboto-Regular", 15f);
            Telefono.Font = UIFont.FromName("Roboto-Regular", 15f);
            Acquista.Font = UIFont.FromName("Roboto-Regular", 15f);

            FirstProductLabel.Font = UIFont.FromName("Roboto-Regular", 15f);
            FirstProductPrice.Font = UIFont.FromName("Roboto-Regular", 15f);

            SecondProductLabel.Font = UIFont.FromName("Roboto-Regular", 15f);
            SecondProductPrice.Font = UIFont.FromName("Roboto-Regular", 15f);

            ThirdProductLabel.Font = UIFont.FromName("Roboto-Regular", 15f);
            ThirdProductPrice.Font = UIFont.FromName("Roboto-Regular", 15f);

            FourthProductLabel.Font = UIFont.FromName("Roboto-Regular", 15f);
            FourthProductPrice.Font = UIFont.FromName("Roboto-Regular", 15f);

            FifthProductLabel.Font = UIFont.FromName("Roboto-Regular", 15f);
            FifthProductPrice.Font = UIFont.FromName("Roboto-Regular", 15f);

            UITapGestureRecognizer FirstProductClick = new UITapGestureRecognizer(() =>
            {

                UIAlertView alert = new UIAlertView("Purchase", "Do you want to buy "+ Prodotti[0].Title+" for "+ Prodotti[0].FormattedPrice, null, "Cancel", "Buy");
                //Wireup events
                alert.CancelButtonIndex = 0;
                alert.Clicked += (caller, buttonArgs) => {
                    // Does the user want to purchase?
                    if (buttonArgs.ButtonIndex == 1)
                    {
                        //Yes, request purchase item	
                        //button.Enabled = false;
                        Acquisto = 1;
                        purchaseManager.BuyProduct(Prodotti[0]);
                    }
                };

                alert.Show();
            });
            FirstProduct.UserInteractionEnabled = true;
            FirstProduct.AddGestureRecognizer(FirstProductClick);

            UITapGestureRecognizer SecondProductClick = new UITapGestureRecognizer(() =>
            {

                UIAlertView alert = new UIAlertView("Purchase", "Do you want to buy " + Prodotti[1].Title + " for " + Prodotti[1].FormattedPrice, null, "Cancel", "Buy");
                //Wireup events
                alert.CancelButtonIndex = 0;
                alert.Clicked += (caller, buttonArgs) => {
                    // Does the user want to purchase?
                    if (buttonArgs.ButtonIndex == 1)
                    {
                        //Yes, request purchase item	
                        //button.Enabled = false;
                        Acquisto = 2;
                        purchaseManager.BuyProduct(Prodotti[1]);
                    }
                };

                alert.Show();

            });
            SecondProduct.UserInteractionEnabled = true;
            SecondProduct.AddGestureRecognizer(SecondProductClick);

            UITapGestureRecognizer ThirdProductClick = new UITapGestureRecognizer(() =>
            {

                UIAlertView alert = new UIAlertView("Purchase", "Do you want to buy " + Prodotti[2].Title + " for " + Prodotti[2].FormattedPrice, null, "Cancel", "Buy");                //Wireup events
                alert.CancelButtonIndex = 0;
                alert.Clicked += (caller, buttonArgs) => {
                    // Does the user want to purchase?
                    if (buttonArgs.ButtonIndex == 1)
                    {
                        //Yes, request purchase item	
                        //button.Enabled = false;
                        Acquisto = 3;
                        purchaseManager.BuyProduct(Prodotti[2]);

                    }
                };

                alert.Show();

            });
            ThirdProduct.UserInteractionEnabled = true;
            ThirdProduct.AddGestureRecognizer(ThirdProductClick);

            UITapGestureRecognizer FourthProductClick = new UITapGestureRecognizer(() =>
            {

                UIAlertView alert = new UIAlertView("Purchase", "Do you want to buy " + Prodotti[3].Title + " for " + Prodotti[3].FormattedPrice, null, "Cancel", "Buy");                //Wireup events
                alert.CancelButtonIndex = 0;
                alert.Clicked += (caller, buttonArgs) => {
                    // Does the user want to purchase?
                    if (buttonArgs.ButtonIndex == 1)
                    {
                        //Yes, request purchase item	
                        //button.Enabled = false;
                        Acquisto = 4;
                        purchaseManager.BuyProduct(Prodotti[3]);
                    }
                };

                alert.Show();

            });
            FourthProduct.UserInteractionEnabled = true;
            FourthProduct.AddGestureRecognizer(FourthProductClick);

            UITapGestureRecognizer FifthProductClick = new UITapGestureRecognizer(() =>
            {

                UIAlertView alert = new UIAlertView("Purchase", "Do you want to buy " + Prodotti[4].Title + " for " + Prodotti[4].FormattedPrice, null, "Cancel", "Buy");                //Wireup events
                alert.CancelButtonIndex = 0;
                alert.Clicked += (caller, buttonArgs) => {
                    // Does the user want to purchase?
                    if (buttonArgs.ButtonIndex == 1)
                    {
                        //Yes, request purchase item	
                        //button.Enabled = false;
                        Acquisto = 5;
                        purchaseManager.BuyProduct(Prodotti[4]);
                    }
                };

                alert.Show();

            });
            FifthProduct.UserInteractionEnabled = true;
            FifthProduct.AddGestureRecognizer(FifthProductClick);

            string messaggiInviati = "23";
            string messaggiRimasti = "7";

            //var client = new RestClient("http://location.keepup.pro:3000/");
            //DISTRIBUTION
            var client = new RestClient("http://api.netwintec.com:3000/");


            var requestN4U = new RestRequest("api/user/" + NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U"), Method.GET);
            requestN4U.AddHeader("content-type", "application/json");

            var response = client.Execute(requestN4U);

            Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {

                JsonValue json = JsonValue.Parse(response.Content);

                Nome.Text = json["name"];
                Telefono.Text = json["phone"];
                try
                {
                    int value = json["credit"];
                    messaggiRimasti = value.ToString();
                }
                catch (Exception e)
                {
                    messaggiRimasti = "0";
                }
                try
                {
                    int value2 = json["message_count"];
                    messaggiInviati = value2.ToString();
                }
                catch (Exception e)
                {
                    messaggiInviati = "0";
                }
            }
            else
            {

                UIStoryboard storyboard = UIStoryboard.FromName("Main", null);
                UIViewController lp = storyboard.InstantiateViewController("HomePage");
                this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                this.PresentViewController(lp, true, null);
            }

            UIView MessageSend = new UIView(new CGRect(0, View.Frame.Height - 40, View.Frame.Width / 2 - 2, 40));
            MessageSend.BackgroundColor = UIColor.FromRGB(63, 148, 202);

            var size = UIStringDrawing.StringSize("Messaggi Inviati", UIFont.FromName("Roboto-Regular", 15), new CGSize(2000, 2000));
            Console.WriteLine("1:" + size.Height + "  " + size.Width);

            UILabel MessageSendLabel = new UILabel(new CGRect(MessageSend.Frame.Width / 2 - (size.Width / 2), View.Frame.Height - 45 - size.Height, size.Width, size.Height));
            MessageSendLabel.Font = UIFont.FromName("Roboto-Regular", 15);
            MessageSendLabel.Text = "Messaggi Inviati";
            MessageSendLabel.TextColor = UIColor.FromRGB(63, 148, 202);

            size = UIStringDrawing.StringSize(messaggiInviati, UIFont.FromName("Roboto-Regular", 16), new CGSize(2000, 2000));
            Console.WriteLine("2:" + size.Height + "  " + size.Width);

            UILabel MessageSendNumber = new UILabel(new CGRect(MessageSend.Frame.Width / 2 - (size.Width / 2), MessageSend.Frame.Height / 2 - (size.Height / 2), size.Width, size.Height));
            MessageSendNumber.Font = UIFont.FromName("Roboto-Regular", 16);
            MessageSendNumber.Text = messaggiInviati;
            MessageSendNumber.TextColor = UIColor.White;

            UIView MessageLeft = new UIView(new CGRect(View.Frame.Width / 2 + 2, View.Frame.Height - 40, View.Frame.Width / 2 - 2, 40));
            MessageLeft.BackgroundColor = UIColor.FromRGB(87, 196, 241);

            size = UIStringDrawing.StringSize("Messaggi Rimasti", UIFont.FromName("Roboto-Regular", 15), new CGSize(2000, 2000));
            Console.WriteLine("3:" + size.Height + "  " + size.Width);

            UILabel MessageLeftLabel = new UILabel(new CGRect(View.Frame.Width / 2 + 2 + (MessageLeft.Frame.Width / 2 - (size.Width / 2)), View.Frame.Height - 45 - size.Height, size.Width, size.Height));
            MessageLeftLabel.Font = UIFont.FromName("Roboto-Regular", 15);
            MessageLeftLabel.Text = "Messaggi Rimasti";
            MessageLeftLabel.TextColor = UIColor.FromRGB(87, 196, 241);

            size = UIStringDrawing.StringSize(messaggiRimasti, UIFont.FromName("Roboto-Regular", 16), new CGSize(2000, 2000));
            Console.WriteLine("4:" + size.Height + "  " + size.Width);

            UILabel MessageLeftNumber = new UILabel(new CGRect(MessageLeft.Frame.Width / 2 - (size.Width / 2), MessageLeft.Frame.Height / 2 - (size.Height / 2), size.Width, size.Height));
            MessageLeftNumber.Font = UIFont.FromName("Roboto-Regular", 16);
            MessageLeftNumber.Text = messaggiRimasti;
            MessageLeftNumber.TextColor = UIColor.White;


            MessageSend.Add(MessageSendNumber);
            MessageLeft.Add(MessageLeftNumber);

            View.Add(MessageSendLabel);
            View.Add(MessageSend);
            View.Add(MessageLeftLabel);
            View.Add(MessageLeft);



            // IN-APP PURCHASE

            purchaseManager = AppDelegate.Instance.PurchaseManager;
            int i = 0;

            if (purchaseManager.Count != 5)
            {
                FirstProduct.BackgroundColor = UIColor.Gray;
                SecondProduct.BackgroundColor = UIColor.Gray;
                ThirdProduct.BackgroundColor = UIColor.Gray;
                FourthProduct.BackgroundColor = UIColor.Gray;
                FifthProduct.BackgroundColor = UIColor.Gray;

                new UIAlertView("Errore","Errore con AppStore acquisti non disponibili al momento",null,"Ok",null).Show();

                FirstProduct.UserInteractionEnabled = false;
                SecondProduct.UserInteractionEnabled = false;
                ThirdProduct.UserInteractionEnabled = false;
                FourthProduct.UserInteractionEnabled = false;
                FifthProduct.UserInteractionEnabled = false;
            }
            else {
                foreach (InAppProduct product in purchaseManager)
                {
                    Console.WriteLine(product.Title + "|" + product.Description + "|" + product.Price + "|" + product.ProductIdentifier + "|" + product.ProductType + "|" + product.AvailableQuantity);

                    if (product.ProductIdentifier == "com.lastlime.lovie.message.consumable_x10")
                    {
                        FirstProductPrice.Text = product.Price + "€";
                        Prodotti.Add(0,product);
                    }
                    if (product.ProductIdentifier == "com.lastlime.lovie.message.consumable_x50")
                    {
                        SecondProductPrice.Text = product.Price + "€";
                        Prodotti.Add(1, product);
                    }
                    if (product.ProductIdentifier == "com.lastlime.lovie.message.consumable_x200")
                    {
                        ThirdProductPrice.Text = product.Price + "€";
                        Prodotti.Add(2, product);
                    }
                    if (product.ProductIdentifier == "com.lastlime.lovie.message.consumable_x500")
                    {
                        FourthProductPrice.Text = product.Price + "€";
                        Prodotti.Add(3, product);
                    }
                    if (product.ProductIdentifier == "com.lastlime.lovie.message.consumable_x1000")
                    {
                        FifthProductPrice.Text = product.Price + "€";
                        Prodotti.Add(4, product);
                    }

                    i++;
                }
            }
            purchaseManager.ReceivedValidProducts += (products) => {
                // Received valid products from the iTunes App Store,
                // Update the display
                //ReloadData();
            };

            purchaseManager.InAppProductPurchased += (StoreKit.SKPaymentTransaction transaction, InAppProduct product) => {
                // Update list to remove any non-consumable products that were
                // purchased
                
                Console.WriteLine("TRANSACTION:"+transaction.Payment+"|" + transaction.OriginalTransaction + "|" + transaction.TransactionDate + "|" + transaction.TransactionIdentifier+ "|"+ transaction.TransactionReceipt + "|" + transaction.TransactionState);

                int exit = 0;
                while (exit == 0)
                {

                    exit = controlloAcquisto(transaction.TransactionReceipt.ToString(), transaction.TransactionIdentifier,Acquisto);

                }

                if (exit == 1)
                {
                    Console.WriteLine("BUY:" + product.Title + " at " + product.Price);
                    Console.WriteLine("Before Consume:" + purchaseManager.ProductQuantityAvailable("com.lastlime.lovie.message"));

                    if (Acquisto == 1)
                    {
                        purchaseManager.ConsumeProductQuantity("com.lastlime.lovie.message", 10);
                        Acquisto = 0;
                    }

                    if (Acquisto == 2)
                    {
                        purchaseManager.ConsumeProductQuantity("com.lastlime.lovie.message", 50);
                        Acquisto = 0;
                    }

                    if (Acquisto == 3)
                    {
                        purchaseManager.ConsumeProductQuantity("com.lastlime.lovie.message", 200);
                        Acquisto = 0;
                    }

                    if (Acquisto == 4)
                    {
                        purchaseManager.ConsumeProductQuantity("com.lastlime.lovie.message", 500);
                        Acquisto = 0;
                    }

                    if (Acquisto == 5)
                    {
                        purchaseManager.ConsumeProductQuantity("com.lastlime.lovie.message", 1000);
                        Acquisto = 0;
                    }

                    Console.WriteLine("After Consume:" + purchaseManager.ProductQuantityAvailable("com.lastlime.lovie.message"));

                }
                if (exit == 2)
                {

                    var adErr = new UIAlertView("Errore", "Pagamento non valido riprovare con credenziali corrette", null, "OK", null);
                    adErr.Show();

                }
            };

            purchaseManager.InAppPurchasesRestored += (count) => {
                // Update list to remove any non-consumable products that were
                // purchased and restored
                //ReloadData();
            };

            purchaseManager.TransactionObserver.InAppPurchaseContentDownloadInProgress += (download) => {
                // Update the table to display the status of any downloads of hosted content
                // that we currently have in progress so we are forcing a table reload on the
                // download progress update. Since the final message will be the raising of the
                // InAppProductPurchased event, we'll just trap it to clear any completed
                // downloads instead of listening to the InAppPurchaseContentDownloadCompleted on the
                // purchase managers transaction observer.
                //ReloadData();

                // Display download percent in the badge
                //StoreTab.BadgeValue = string.Format("{0:###}%", _purchaseManager.ActiveDownloadPercent * 100.0f); ;
            };

            purchaseManager.TransactionObserver.InAppPurchaseContentDownloadCompleted += (download) => {
                // Clear badge
                //StoreTab.BadgeValue = null;
            };

            purchaseManager.TransactionObserver.InAppPurchaseContentDownloadCanceled += (download) => {
                // Clear badge
                //StoreTab.BadgeValue = null;
            };

            purchaseManager.TransactionObserver.InAppPurchaseContentDownloadFailed += (download) => {
                // Clear badge
                //StoreTab.BadgeValue = null;
            };

            purchaseManager.TransactionObserver.InAppPurchaseContentDownloadFailed += (download) => {
                // Inform the user that the download has failed. Normally download would contain
                // information about the failure that you would want to display to the user, since
                // we are running in simulation mode download will be null, so just display a 
                // generic failure message.
                /*using (var alert = new UIAlertView("Download Failed", "Unable to complete the downloading of content for the product being purchased. Please try again later.", null, "OK", null))
                {
                    alert.Show();
                }

                // Force the table to reload to remove current download message
                ReloadData();*/
            };

            purchaseManager.InAppProductPurchaseFailed += (transaction, product) => {
                // Inform caller that the purchase of the requested product failed.
                // NOTE: The transaction will normally encode the reason for the failure but since
                // we are running in the simulated iTune App Store mode, no transaction will be returned.
                //Display Alert Dialog Box
                using (var alert = new UIAlertView("Lovie", String.Format("Il tentativo di comprare {0} è fallito: {1}", product.Title, transaction.Error.ToString()), null, "OK", null))
                {
                    alert.Show();
                }

                // Force a reload to clear any locked items
                //ReloadData();*/
            };
        }

        public void CompareNotification()
        {

            CABasicAnimation AlphaIn = new CABasicAnimation();
            AlphaIn.KeyPath = "opacity";
            AlphaIn.BeginTime = 0;
            AlphaIn.Duration = 1.5;
            AlphaIn.From = new NSNumber(0);
            AlphaIn.To = new NSNumber(1);

            CABasicAnimation AlphaOut = new CABasicAnimation();
            AlphaOut.KeyPath = "opacity";
            AlphaOut.BeginTime = 1.5;
            AlphaOut.Duration = 1.5;
            AlphaOut.From = new NSNumber(1);
            AlphaOut.To = new NSNumber(0);

            CAAnimationGroup animationGroup = CAAnimationGroup.CreateAnimation();
            animationGroup.Duration = 3;
            animationGroup.RepeatCount = 1;
            animationGroup.Animations = new CAAnimation[] { AlphaIn, AlphaOut };

            NotView.Layer.AddAnimation(animationGroup, null);
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Logout()
        {
            UIStoryboard storyboard;
            LoginManager loginManager = new LoginManager();
            int button = await ShowAlert("Logout", "Vuoi effettuare il Logout", "Ok", "Annulla");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {
                loginManager.LogOut();

                //******** DELETE TOKEN PUSH *******
                try
                {

                    //DEVELOPMENT
                    //var client = new RestClient("http://location.keepup.pro:3000/");
                    //DISTRIBUTION
                    var client = new RestClient("http://api.netwintec.com:3000/");

                    Console.WriteLine(NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U")+"|"+ UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString());

                    var requestN4UPush = new RestRequest("api/logout", Method.POST);
                    requestN4UPush.AddHeader("content-type", "application/json");
                    requestN4UPush.AddHeader("lovie-token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U"));
                    requestN4UPush.Timeout = 60000;

                    JObject oJsonObject = new JObject();

                    oJsonObject.Add("device_uuid", UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString());
                    

                    requestN4UPush.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


                    client.ExecuteAsync(requestN4UPush, (s, e) =>
                    {

                        Console.WriteLine("Logout" + s.Content + "!" + s.StatusCode);

                    });

                }
                catch (Exception e)
                {
                    Console.WriteLine("Aaaa");
                }
                //************************

                storyboard = UIStoryboard.FromName("Main", null);

                NSUserDefaults.StandardUserDefaults.SetString("", "TokenLovieFB");
                NSUserDefaults.StandardUserDefaults.SetString("", "TokenLovieN4U");
                UIViewController lp = storyboard.InstantiateViewController("LoginPage");
                this.PresentModalViewController(lp, true);
            }
            else { }
        }

        public int controlloAcquisto(string data,string id,int n)
        {

            //DEVELOPMENT
            //var client = new RestClient("http://location.keepup.pro:3000/");
            //DISTRIBUTION
            var client = new RestClient("http://api.netwintec.com:3000/");


            var requestN4U = new RestRequest("api/user/update_credit", Method.POST);
            requestN4U.AddHeader("content-type", "application/json");
            requestN4U.AddHeader("lovie-token", NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U"));

            JObject oJsonObject = new JObject();

            oJsonObject.Add("data", data);
            oJsonObject.Add("type", "ios");
            /*oJsonObject.Add("orderId", id);

            if (Acquisto == 1)
            {
                oJsonObject.Add("productId", "com.lastlime.lovie.message.consumable_x10");
            }

            if (Acquisto == 2)
            {
                oJsonObject.Add("productId", "com.lastlime.lovie.message.consumable_x50");
            }

            if (Acquisto == 3)
            {
                oJsonObject.Add("productId", "com.lastlime.lovie.message.consumable_x200");
            }

            if (Acquisto == 4)
            {
                oJsonObject.Add("productId", "com.lastlime.lovie.message.consumable_x500");
            }

            if (Acquisto == 5)
            {
                oJsonObject.Add("productId", "com.lastlime.lovie.message.consumable_x1000");
            }
            */
            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


            var response = client.Execute(requestN4U);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {

                /*if (response.Content == "false")
                {
                    return 2;
                }
                if (response.Content == "true")
                {
                    return 1;
                }*/

                return 1;

            }

            if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {

                /*if (response.Content == "false")
                {
                    return 2;
                }
                if (response.Content == "true")
                {
                    return 1;
                }*/

                return 2;

            }
            return 0;

        }
    }
}
