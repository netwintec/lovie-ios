﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UIKit;

namespace Lovie
{
    class CustomCellChat : UITableViewCell
    {
        UILabel nomeLabel, messaggioLabel,dataLabel;
        UIImageView imageView;
        UIView separator,circle;
        bool isRead;
        public CustomCellChat(NSString cellId,bool read) : base(UITableViewCellStyle.Default, cellId)
        {

            isRead = read;

            SelectionStyle = UITableViewCellSelectionStyle.Gray;
            //ContentView.BackgroundColor = UIColor.FromRGB(218, 255, 127);
            imageView = new UIImageView();

            separator = new UIView();
            separator.BackgroundColor = UIColor.FromRGB(0, 160, 210);

            circle = new UIView();
            if (isRead) {
                circle.BackgroundColor = UIColor.Clear;
            }
            else {
                circle.BackgroundColor = UIColor.FromRGB(87, 196, 241);
            }

            nomeLabel = new UILabel()
            {
                //Font = UIFont.FromName("Roboto-Regular", 16.5f),
                TextColor = UIColor.FromRGB(0, 0, 0),
                TextAlignment = UITextAlignment.Left,
                BackgroundColor = UIColor.Clear

            };

            if (isRead)
            {
                nomeLabel.Font = UIFont.FromName("Roboto-Regular", 16.5f);
            }
            else
            {
                nomeLabel.Font = UIFont.FromName("Roboto-Bold", 16.5f);
            }

            messaggioLabel = new UILabel()
            {
                
                TextColor = UIColor.FromRGB(80,80,80),
                Lines = 1,
                TextAlignment = UITextAlignment.Left,
                BackgroundColor = UIColor.Clear
                
            };

            if (isRead)
            {
                messaggioLabel.Font = UIFont.FromName("Roboto-Regular", 11f);
            }
            else
            {
                messaggioLabel.Font = UIFont.FromName("Roboto-Bold", 12f);
            }

            dataLabel = new UILabel()
            {
                Font = UIFont.FromName("Roboto-Regular", 11f),
                Lines = 1,
                TextColor = UIColor.FromRGB(80, 80, 80),
                TextAlignment = UITextAlignment.Right,
                BackgroundColor = UIColor.Clear

            };

            ContentView.AddSubviews(new UIView[] { nomeLabel, messaggioLabel,dataLabel, imageView, circle, separator });

        }
        public void UpdateCell(string nome, string messaggio,string data, string image)
        {
            imageView.Image = UIImage.FromFile("profilePicture.png");
            nomeLabel.Text = nome;
            messaggioLabel.Text = messaggio;
            dataLabel.Text = data;

            if (image.CompareTo("standard") != 0)
            {
                SetImageAsync(imageView, image);
            }

        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            nfloat widht = ContentView.Frame.Width;
            nfloat height = ContentView.Frame.Height;

            imageView.Frame = new CGRect(15, 15, 45, 45);
            imageView.Layer.CornerRadius = 22.5f;
            imageView.Layer.MasksToBounds = true;
            nomeLabel.Frame = new CGRect(70, 18, widht - 125, 20);
            messaggioLabel.Frame = new CGRect(70, 43, widht - 165, 14);
            dataLabel.Frame = new CGRect(widht-85, 43, 70, 14);

            circle.Frame = new CGRect(85 + (widht - 125), 23, 10, 10);
            circle.Layer.CornerRadius = 5;
            separator.Frame = new CGRect(70, ContentView.Frame.Height - 2, ContentView.Frame.Width - 85, 2);
        }

        public void SetImageAsync(UIImageView image, string url)
        {
            UIImage Placeholder = UIImage.FromFile("profilePicture.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(url) ?? Placeholder;

                    InvokeOnMainThread(() =>
                    {

                        image.Image = cachedImage;

                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }


        static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                if (data != null)
                    return UIImage.LoadFromData(data);
            return null;
        }
    }
}
