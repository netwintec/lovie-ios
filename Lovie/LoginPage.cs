﻿using CoreGraphics;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using UIKit;
using CoreAnimation;
using RestSharp;
using System.Json;
using Newtonsoft.Json.Linq;

namespace Lovie
{
	partial class LoginPage : UIViewController
	{

        static string TXT_EULA = "Termini e condizioni di uso della App di Lovie\n\n1. Uso dell’App Lovie\nScaricando, installando e utilizzando l’APP Lovie l’utente accetta i seguenti termini e condizioni, nonché qualsiasi modifica che potrà essere apportata.\nAl fine di accedere ai servizi e usare l’APP Lovie l’utente potrà essere tenuto a fornire i dati occorrenti per la propria identificazione o,comunque, i dati occorrenti per consentire l’uso dell’APP Lovie e dei servizi ad essa associati. Lovie non potrà essere ritenuta responsabile per qualsiasi perdita o danno derivante dalla comunicazione di dati e di informazioni non veritiere.\nL‘APP Lovie potrà visualizzare annunci pubblicitari e promozioni sia di Lovie stessa, sia di società ad essa collegate, sia di terzi.\nL’utente accetta che Lovie possa inserire tale pubblicità e che non sia ritenuta responsabile per qualsiasi perdita o danno di alcun tipo subito dall’utente a seguito della presenza di tali inserzionisti o dei conseguenti rapporti dell’utente con tali inserzionisti.\nL’APP Lovie potrà fornire link a siti o risorse web. L’utente riconosce e accetta quindi che Lovie non è responsabile per la disponibilità di tali risorse o siti esterni e che non avalla e non è responsabile per i relativi contenuti, la pubblicità, i prodotti o altri materiali presenti o disponibili su tali siti o risorse.\nL’utente e Lovie potranno interrompere l’uso dell’APP Lovie in qualsiasi momento. Lovie si riserva il diritto di modificare, sospendere o interrompere i servizi forniti attraverso l’APP Lovie o parte di essi, in qualsiasi momento e con o senza preavviso. \nL’utente accetta che Lovie non sarà ritenuta responsabile nei propri confronti o di qualsiasi terza parte per le predette modifiche, le sospensioni o le interruzioni.\nL’APP può essere scaricata e utilizzata solo da soggetti di maggiore età: l’utente pertanto dovrà avere un età di almeno 18 anni.\n\n2. Servizi fruibili tramite l’APP di Lovie\nLovie rende fruibili tramite la propria APP, i servizi di chat con utenti Facebook e Whatsapp.\nL’utente ha l’obbligo di utilizzare il servizio nel rispetto delle leggi, dei regolamenti tempo per tempo vigenti, astenendosi dall’inviare messaggio di contenuto illegale o illecito.\nL’utente, in particolare e senza che ciò implichi alcuna deroga a quanto sopra, prende atto e accetta che:\na) i servizi devono essere utilizzati nel rispetto dei principi di buona fede e correttezza;\nb) fatto divieto di usufruire dei servizi  per scopi o attività illecite o fraudolente;\nc) Lovie è unicamente responsabile della fornitura dei servizi di accesso.\nPer poter utilizzare i servizi in piena sicurezza dall’APP Lovie l’utente dovrà registrarsi utilizzando un proprio codice di sicurezza secondo le modalità e le indicazioni fornite direttamente durante la procedura di download ovvero specificate sul sito www.lovie.it.\nL’utente è l’unico responsabile del corretto e personale uso dell’APP e della relativa password, della sua custodia e dell’inaccessibilità a terzi, nonché della custodia e del corretto uso del terminale.\nL’utente è pertanto tenuto a custodire il terminale e la proprio password con particolare attenzione, di non cedere o dare in uso a terzi il proprio terminale, di non comunicare o trascrivere la propria password su alcun supporto, nonché di tenerlo separato dal terminale.\nIn caso di fondato sospetto che terzi siano venuti a conoscenza della propria Password, è necessario provvedere all’immediata modifica dello stesso.\nIn caso di dismissione o cessione a terzi del proprio terminale, l’utente dovrà rimuovere sempre l’APP dal terminale.\nL’utente infine prende atto e accetta che, al fine di tutelare l’integrità delle reti, la sicurezza del servizio ed evitare frodi o utilizzi anomali dei servizi, Lovie si riserva la facoltà di adottare appropriati interventi, ivi incluse limitazioni o sospensioni della fruizione dei SMD stessi.\n\n3. Norme sulla privacy\nInstallando l’APP Lovie l’utente dichiara e accetta che Lovie possa accedere, conservare e trattare i dati personali dell’utente e qualsiasi contenuto associato all’uso dell’APP, se richiesto per legge o se tali attività siano necessarie per l’uso dell’APP Lovie ovvero dei servizi forniti attraverso l’APP, nel pieno rispetto dei principi dettati dal D. lgs. 196/03 “Codice in materia di protezione dei dati personali”\n(“Codice”). L’utente riconosce che l’uso dell’APP Lovie può prevedere la trasmissione di dati attraverso reti di comunicazione elettronica.\nI dati trattati sono quelli necessari al corretto funzionamento dell’APP e dei relativi servizi, nello specifico i dati obbligatori richiesti sono: Nome, Cognome, Telefono e Immagine del profilo. Questi dati possono essere presi anche da Facebook qualora l’utente si registri, dando il consenso, con il proprio utente e password di Facebook. Con il consenso dell’utente Lovie potrà inoltre, inviare comunicazioni aventi ad oggetto offerte e/o promozioni di Lovie e/o di partner di Lovie  con le modalità e le forme previste dall’art. 130 del Codice.\nLovie non potrà essere ritenuta responsabile per qualsiasi perdita o danno derivante dalla comunicazione di tali dati.\nL’utente può venire a conoscenza a quindi trattare dati di terzi (foto, video, messaggi, esperienze, eventi, incontri, preferenze etc..): tali dati andranno trattati nel rispetto del d. lgs. 196/2003 e delle leggi civili e penali. L’utente è il titolare di tale trattamento e l’unico ed esclusivo responsabile.\nLovie non conosce i contenuti delle comunicazioni o dei messaggi scambiate dagli utenti, può in caso di richiesta da parte delle autorità fornire copia delle specifiche comunicazioni che sono conservate sui nostri sistemi per un tempo illimitato.";

        UIStoryboard storyboard = UIStoryboard.FromName("Main", null);
        private float _scrollAmount;
        private double _animationDuration;
        private UIViewAnimationCurve _animationCurve;
        LoginButton loginFbButton;
        LoginManager loginManager;
        List<string> readPermissions = new List<string> { "public_profile","email", "user_birthday" };

        bool PlaceholderTextP = true;
        bool PlaceholderTextC = true;
        bool PlaceholderTextPr = false;
        bool PlaceholderTextNome = true;
        bool PlaceholderTextCogn = true;

        bool KeyOpen = false;
        bool numberInsert = false;
        bool Registrato = false;

        public string tokenAppoggio;
        public string tokenfbAppoggio;

        string NomeText, CognomeText;

        UIScrollView svEULA;

        public LoginPage (IntPtr handle) : base (handle)
		{
		}
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();


            Profile.Notifications.ObserveDidChange((sender, e) =>
            {

                if (e.NewProfile == null)
                    return;

                //nameLabel.Text = e.NewProfile.Name;
            });

            // Set the Read and Publish permissions you want to get
            loginFbButton = new LoginButton(new CGRect(0, 0, 180, 40))//AccediText.Frame.X+(AccediText.Frame.Width/2) - 60, AccediText.Frame.Y, 120, 40))
            {
                
                LoginBehavior = LoginBehavior.Native,
                ReadPermissions = readPermissions.ToArray(),
                //Font = UIFont.FromName("Roboto-Regular", 12)
            };


            loginManager = new LoginManager();



            // Handle actions once the user is logged in
            loginFbButton.Completed += (sender, e) =>
            {
                if (e.Error != null)
                {
                    var adErr = new UIAlertView("Login Non Riuscito", "E-Mail o Password Errata", null, "OK", null);
                    adErr.Show();
                    // Handle if there was an error
                }

                if (e.Result != null)
                {
                    if (e.Result.IsCancelled)
                    {
                        // Handle if the user cancelled the login request
                    }
                    else
                    {
                        if (e.Result.Token.TokenString != null)
                        {
                            Console.WriteLine(e.Result.Token.TokenString);


                            BottomFBView.Alpha = 0;
                            BottomLoadView.Alpha = 1;
                            BottomCheckView.Alpha = 0;

                            //DEVELOPMENT
                            //var client = new RestClient("http://location.keepup.pro:3000/");
                            //DISTRIBUTION
                            var client = new RestClient("http://api.netwintec.com:3000/");


                            var requestN4U = new RestRequest("api/login/facebook", Method.POST);
                            requestN4U.AddHeader("content-type", "application/json");

                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("access_token", e.Result.Token.TokenString);
                            oJsonObject.Add("device_token", AppDelegate.Instance.APNToken);
                            oJsonObject.Add("device_type", "ios");
                            oJsonObject.Add("device_uuid", UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString());

                            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


                            client.ExecuteAsync(requestN4U, (s, re) =>
                            {

                                Console.WriteLine("RESPONSE" + s.Content + "|" + s.StatusCode);



                                if (s.StatusCode == System.Net.HttpStatusCode.OK)
                                {

                                    InvokeOnMainThread(() =>
                                    {

                                        JsonValue json = JsonValue.Parse(s.Content);
                                        //Console.WriteLine (response.Content);
                                        string token = json["token"];
                                        int verified = json["verified"];

                                        Console.WriteLine(token + "|" + verified);

                                        if (verified == 0)
                                        {

                                            BottomFBView.Alpha = 0;
                                            BottomLoadView.Alpha = 0;
                                            BottomCheckView.Alpha = 1;

                                            tokenAppoggio = token;
                                            tokenfbAppoggio = e.Result.Token.TokenString;

                                            PhoneView.Alpha = 0;
                                            CodeText.Enabled = false;
                                        }
                                        else
                                        {

                                            NSUserDefaults.StandardUserDefaults.SetString(token, "TokenLovieN4U");
                                            NSUserDefaults.StandardUserDefaults.SetString(e.Result.Token.TokenString, "TokenLovieFB");

                                            UIViewController lp = storyboard.InstantiateViewController("HomePage");
                                            this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                                            this.PresentViewController(lp, true, null);

                                        }


                                    });

                                }
                                else
                                {

                                    Console.WriteLine("1a:" + (s.StatusCode != 0));
                                    InvokeOnMainThread(() =>
                                    {

                                        loginManager.LogOut();

                                        BottomFBView.Alpha = 1;
                                        BottomLoadView.Alpha = 0;
                                        BottomCheckView.Alpha = 0;


                                        var adErr = new UIAlertView("Errore Login", "Login non riuscito provate più tardi", null, "OK", null);
                                        adErr.Show();
                                        Console.WriteLine("1b");
                                    });

                                }
                            });

                            LoadAnim.StartAnimating();

                        }
                    }
                }
                // Handle your successful login
            };

            // Handle actions once the user is logged out
            loginFbButton.LoggedOut += (sender, e) =>
            {
                // Handle your logout
                // nameLabel.Text = "";
            };

            FBView.Add(loginFbButton);


            SendButton.TouchUpInside += SendButton_TouchUpInside;


            UITapGestureRecognizer ViewTap = new UITapGestureRecognizer(() =>
            {
                if (PhoneText.IsEditing)
                {
                    PhoneText.ResignFirstResponder();
                    ScrollTheView(false);
                }

                if (PrefixText.IsEditing)
                {
                    PrefixText.ResignFirstResponder();
                    ScrollTheView(false);
                }

                if (CodeText.IsEditing)
                {
                    CodeText.ResignFirstResponder();
                    ScrollTheView(false);
                }
            });
            ContentView.UserInteractionEnabled = true;
            ContentView.AddGestureRecognizer(ViewTap);


            UITapGestureRecognizer AvantiTap = new UITapGestureRecognizer(() =>
            {
                if (Nome.IsEditing)
                {
                    Nome.ResignFirstResponder();
                    ScrollTheView(false);
                }

                if (Cognome.IsEditing)
                {
                    Cognome.ResignFirstResponder();
                    ScrollTheView(false);
                }

                if (!PlaceholderTextNome && !PlaceholderTextCogn && !string.IsNullOrWhiteSpace(Nome.Text) && !string.IsNullOrWhiteSpace(Cognome.Text))
                {

                    NomeText = Nome.Text;
                    CognomeText = Cognome.Text;

                    BottomFBView.Alpha = 0;
                    BottomLoadView.Alpha = 0;
                    BottomCheckView.Alpha = 1;

                    PhoneView.Alpha = 0;
                    CodeText.Enabled = false;

                    Registrato = true;

                }


            });
            avanti.UserInteractionEnabled = true;
            avanti.AddGestureRecognizer(AvantiTap);

            //GESTIONE TEXTVIEW

            PhoneText.Text = "Scrivi il numero";

            PhoneText.TextColor = UIColor.FromRGB(80, 80, 80);
            PhoneText.Font = UIFont.FromName("Roboto-Regular", 15);
            //message.BorderStyle = UITextBorderStyle.None;

            PhoneText.ShouldBeginEditing += (UITextField) => {
                if (PhoneText.Text == "Scrivi il numero")
                {
                    PhoneText.Text = "";
                    PhoneText.TextColor = UIColor.Black;
                }
                PlaceholderTextP = false;
                return true;
            };

            PhoneText.ShouldEndEditing += (UITextField) => {           
                if (string.IsNullOrEmpty(PhoneText.Text) || string.IsNullOrWhiteSpace(PhoneText.Text))
                {
                    PhoneText.Text = "Scrivi il numero";
                    PlaceholderTextP = true;
                    PhoneText.TextColor = UIColor.FromRGB(80, 80, 80);
                }

                return true;
            };

            
            
            PhoneText.ShouldReturn += (UITextField) =>
            {
                
                UITextField.ResignFirstResponder();
                ScrollTheView(false);
                KeyOpen = false;
                return true;

            };

            //PrefixText.Text = "Scrivi il numero";

            PrefixText.TextColor = UIColor.Black;
            PrefixText.Font = UIFont.FromName("Roboto-Regular", 11);
            //message.BorderStyle = UITextBorderStyle.None;

            PrefixText.ShouldBeginEditing += (UITextField) => {
                if (PrefixText.Text == "Pref")
                {
                    PrefixText.Text = "";
                    PrefixText.TextColor = UIColor.Black;
                }
                PlaceholderTextPr = false;
                return true;
            };

            PrefixText.ShouldEndEditing += (UITextField) => {
                if (string.IsNullOrEmpty(PrefixText.Text) || string.IsNullOrWhiteSpace(PrefixText.Text))
                {
                    PrefixText.Text = "Pref";
                    PlaceholderTextPr = true;
                    PrefixText.TextColor = UIColor.FromRGB(80, 80, 80);
                }
                return true;
            };



            PrefixText.ShouldReturn += (UITextField) =>
            {

                UITextField.ResignFirstResponder();
                ScrollTheView(false);
                KeyOpen = false;
                return true;

            };

            CodeText.Text = "Scrivi il codice";

            CodeText.TextColor = UIColor.FromRGB(80, 80, 80);
            CodeText.Font = UIFont.FromName("Roboto-Regular", 15);
            //message.BorderStyle = UITextBorderStyle.None;

            CodeText.ShouldBeginEditing += (UITextField) => {
                if (CodeText.Text == "Scrivi il codice")
                {
                    CodeText.Text = "";
                    CodeText.TextColor = UIColor.Black;
                }
                PlaceholderTextC = false;
                return true;
            };

            CodeText.ShouldEndEditing += (UITextField) => {
                if (string.IsNullOrEmpty(PhoneText.Text) || string.IsNullOrWhiteSpace(PhoneText.Text))
                {
                    CodeText.Text = "Scrivi il codice";
                    PlaceholderTextC = true;
                    CodeText.TextColor = UIColor.FromRGB(80, 80, 80);
                }
                KeyOpen = false;
                return true;
            };



            CodeText.ShouldReturn += (UITextField) =>
            {

                UITextField.ResignFirstResponder();
                ScrollTheView(false);
                return true;

            };

            Nome.Text = "Nome";

            Nome.TextColor = UIColor.FromRGB(80, 80, 80);
            Nome.Font = UIFont.FromName("Roboto-Regular", 15);
            //message.BorderStyle = UITextBorderStyle.None;

            Nome.ShouldBeginEditing += (UITextField) => {
                if (Nome.Text == "Nome")
                {
                    Nome.Text = "";
                    Nome.TextColor = UIColor.Black;
                }
                PlaceholderTextNome = false;
                return true;
            };

            Nome.ShouldEndEditing += (UITextField) => {
                if (string.IsNullOrEmpty(Nome.Text) || string.IsNullOrWhiteSpace(Nome.Text))
                {
                    PhoneText.Text = "Nome";
                    PlaceholderTextNome = true;
                    PhoneText.TextColor = UIColor.FromRGB(80, 80, 80);
                }

                return true;
            };



            Nome.ShouldReturn += (UITextField) =>
            {

                UITextField.ResignFirstResponder();
                ScrollTheView(false);
                KeyOpen = false;
                return true;

            };

            Cognome.Text = "Cognome";

            Cognome.TextColor = UIColor.FromRGB(80, 80, 80);
            Cognome.Font = UIFont.FromName("Roboto-Regular", 15);
            //message.BorderStyle = UITextBorderStyle.None;

            Cognome.ShouldBeginEditing += (UITextField) => {
                if (Cognome.Text == "Cognome")
                {
                    Cognome.Text = "";
                    Cognome.TextColor = UIColor.Black;
                }
                PlaceholderTextCogn = false;
                return true;
            };

            Cognome.ShouldEndEditing += (UITextField) => {
                if (string.IsNullOrEmpty(Cognome.Text) || string.IsNullOrWhiteSpace(Cognome.Text))
                {
                    Cognome.Text = "Cognome";
                    PlaceholderTextCogn = true;
                    Cognome.TextColor = UIColor.FromRGB(80, 80, 80);
                }

                return true;
            };



            Cognome.ShouldReturn += (UITextField) =>
            {

                UITextField.ResignFirstResponder();
                ScrollTheView(false);
                KeyOpen = false;
                return true;

            };

            NSObject _notification = UIKeyboard.Notifications.ObserveWillShow((s, e) =>
            {
                _animationDuration = e.AnimationDuration;
                _animationCurve = e.AnimationCurve;

                var r = UIKeyboard.FrameBeginFromNotification(e.Notification);
                _scrollAmount = (float)r.Height;
                ScrollTheView(true);
            });

            // EULA VIEW

            var sizeTXT = UIStringDrawing.StringSize("Accedendo all'app accetti i", UIFont.FromName("Roboto-Regular", 12), new CGSize(TermView.Frame.Width, 2000));
            var sizeBTN = UIStringDrawing.StringSize("termini di utilizzo dell'app", UIFont.FromName("Roboto-Regular", 12), new CGSize(TermView.Frame.Width, 2000));

            float totalW = (float)(sizeTXT.Width + sizeBTN.Width+2);

            UILabel TermText = new UILabel(new CGRect(TermView.Frame.Width / 2 - totalW / 2, 0, sizeTXT.Width, sizeTXT.Height));
            TermText.Font = UIFont.FromName("Roboto-Regular", 12);
            TermText.Text = "Accedendo all'app accetti i";
            TermText.TextColor = UIColor.Black;
            
            UIButton TermButton = new UIButton(new CGRect((TermView.Frame.Width / 2 - totalW / 2)+ sizeTXT.Width+ 2, 0, sizeBTN.Width, sizeBTN.Height));
            TermButton.Font = UIFont.FromName("Roboto-Regular", 12);
            TermButton.SetTitle("termini di utilizzo dell'app", UIControlState.Normal);
            TermButton.SetTitleColor(UIColor.FromRGB(63, 148, 202), UIControlState.Normal);

            UIView Underline = new UIView(new CGRect((TermView.Frame.Width / 2 - totalW / 2) + sizeTXT.Width + 2, sizeBTN.Height, sizeBTN.Width, 1));
            Underline.BackgroundColor = UIColor.FromRGB(63, 148, 202);

            var size = UIStringDrawing.StringSize(TXT_EULA, UIFont.FromName("Roboto-Regular", 15), new CGSize(TextView.Frame.Width - 10, 100000));

            ChiudiButton.Font = UIFont.FromName("Roboto-Regular", 16);

            ChiudiView.Layer.CornerRadius = (nfloat)17.5;

            svEULA = new UIScrollView(new CGRect(0, 0, TextView.Frame.Width, TextView.Frame.Height));

            UILabel txtEULA = new UILabel(new CGRect(5,5, TextView.Frame.Width -10, size.Height));
            txtEULA.Text = TXT_EULA;
            txtEULA.TextColor = UIColor.Black;
            txtEULA.Font = UIFont.FromName("Roboto-Regular", 15);
            txtEULA.Lines = 0;

            svEULA.ContentSize= new CGSize(TextView.Frame.Width - 10, size.Height +10);

            svEULA.Add(txtEULA);
            TextView.Add(svEULA);

            TermView.Add(Underline);
            TermView.Add(TermButton);
            TermView.Add(TermText);

            TermButton.TouchUpInside += delegate {

                PopUpView.Alpha = 1;
                svEULA.ScrollRectToVisible(new CGRect(0, 0, 10, 10), false);

            };

            ChiudiButton.TouchUpInside += delegate {

                PopUpView.Alpha = 0;

            };
        }

        private void SendButton_TouchUpInside(object sender, EventArgs e)
        {

            if (PhoneText.IsEditing)
            {
                PhoneText.ResignFirstResponder();
                ScrollTheView(false);
            }

            if (CodeText.IsEditing)
            {
                CodeText.ResignFirstResponder();
                ScrollTheView(false);
            }


            if (!numberInsert && !string.IsNullOrWhiteSpace(PhoneText.Text) && !string.IsNullOrWhiteSpace(PrefixText.Text) && !PlaceholderTextPr && !PlaceholderTextP)
            {

                bool numberValid = true;

                //string sub = PhoneText.Text.Substring(1);

                try
                {

                    Int64 numValid = Int64.Parse(PhoneText.Text);

                }
                catch (Exception ee)
                {

                    numberValid = false;

                }

                /*if (PhoneText.Text.Length != 10)
                {
                    Console.WriteLine("1");
                    numberValid = false;
                }*/

                if (!PrefixText.Text.Contains("+") && PrefixText.Text.Length <= 1)
                {
                    Console.WriteLine("2");
                    numberValid = false;
                }

                if (numberValid)
                {

                    //********** VERIFICA NUMERO ******* 

                    if (!Registrato)
                    {
                        // DEVELOPMENT 
                        //var client = new RestClient("http://location.keepup.pro:3000/");
                        //DISTRIBUTION
                        var client = new RestClient("http://api.netwintec.com:3000/");


                        var requestN4U = new RestRequest("api/login/get_phone_token", Method.POST);
                        requestN4U.AddHeader("content-type", "application/json");
                        requestN4U.AddHeader("lovie-token", tokenAppoggio);

                        JObject oJsonObject = new JObject();

                        oJsonObject.Add("phone", PrefixText.Text + PhoneText.Text);

                        requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


                        IRestResponse response = client.Execute(requestN4U);

                        Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {

                            CodeView.Alpha = 0;
                            PhoneView.Alpha = 1;
                            PhoneText.Enabled = false;
                            CodeText.Enabled = true;
                            numberInsert = true;
                            SendButton.SetTitle("Verifica il codice", UIControlState.Normal);
                        }
                        else
                        {

                            PhoneText.Text = "";

                            var adErr = new UIAlertView("Errore", "Numero male inserito oppure già presente.", null, "Riprova", null);
                            adErr.Show();

                        }
                    }
                    else
                    {

                        bool errore = false;

                        // DEVELOPMENT 
                        //var client = new RestClient("http://location.keepup.pro:3000/");
                        //DISTRIBUTION
                        var client = new RestClient("http://api.netwintec.com:3000/");


                        var requestN4U = new RestRequest("api/registration ", Method.POST);
                        requestN4U.AddHeader("content-type", "application/json");

                        JObject oJsonObject = new JObject();

                        oJsonObject.Add("name", NomeText+" "+CognomeText);
                        oJsonObject.Add("phone", PrefixText.Text + PhoneText.Text);
                        oJsonObject.Add("device_token", AppDelegate.Instance.APNToken);
                        oJsonObject.Add("device_type", "ios");
                        oJsonObject.Add("device_uuid", UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString());

                        requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


                        IRestResponse response = client.Execute(requestN4U);

                        Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            JsonValue json = JsonValue.Parse(response.Content);
                            string token = json["token"];
                            int verified = json["verified"];

                            if (verified == 0)
                            {
                                CodeView.Alpha = 0;
                                PhoneView.Alpha = 1;
                                PhoneText.Enabled = false;
                                CodeText.Enabled = true;
                                numberInsert = true;
                                SendButton.SetTitle("Verifica il codice", UIControlState.Normal);

                                tokenAppoggio = token;
                            }
                            else
                            {

                                errore = true;
                            }
                        }
                        else
                        {

                            errore = true;

                        }

                        if (errore)
                        {

                            // DEVELOPMENT 
                            //var client = new RestClient("http://location.keepup.pro:3000/");
                            //DISTRIBUTION
                            var client2 = new RestClient("http://api.netwintec.com:3000/");


                            var request2N4U = new RestRequest("api/login/standard ", Method.POST);
                            request2N4U.AddHeader("content-type", "application/json");

                            JObject oJsonObject2 = new JObject();

                            oJsonObject2.Add("name", NomeText + " " + CognomeText);
                            oJsonObject2.Add("phone", PrefixText.Text + PhoneText.Text);
                            oJsonObject2.Add("device_token", AppDelegate.Instance.APNToken);
                            oJsonObject2.Add("device_type", "ios");
                            oJsonObject2.Add("device_uuid", UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString());

                            request2N4U.AddParameter("application/json; charset=utf-8", oJsonObject2, ParameterType.RequestBody);


                            IRestResponse response2 = client2.Execute(request2N4U);

                            Console.WriteLine("RESPONSE:" + response2.Content + "|" + response2.StatusCode);

                            if (response2.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                JsonValue json = JsonValue.Parse(response2.Content);
                                string token = json["token"];
                                int verified = json["verified"];

                                if (verified == 0)
                                {
                                    CodeView.Alpha = 0;
                                    PhoneView.Alpha = 1;
                                    PhoneText.Enabled = false;
                                    CodeText.Enabled = true;
                                    numberInsert = true;
                                    SendButton.SetTitle("Verifica il codice", UIControlState.Normal);

                                    tokenAppoggio = token;
                                }
                                else
                                {

                                    PhoneText.Text = "";

                                    var adErr = new UIAlertView("Errore", "Numero male inserito oppure già presente.", null, "Riprova", null);
                                    adErr.Show();
                                }
                            }
                            else
                            {

                                PhoneText.Text = "";

                                var adErr = new UIAlertView("Errore", "Numero male inserito oppure già presente.", null, "Riprova", null);
                                adErr.Show();

                            }

                        }
                    }

                }

            }

            if (numberInsert && !string.IsNullOrWhiteSpace(CodeText.Text) && !PlaceholderTextC)
            {

                //********** VERIFICA CODICE *******

                //var client = new RestClient("http://location.keepup.pro:3000/");
                //DISTRIBUTION
                var client = new RestClient("http://api.netwintec.com:3000/");


                var requestN4U = new RestRequest("api/login/verify_phone_token", Method.POST);
                requestN4U.AddHeader("content-type", "application/json");
                requestN4U.AddHeader("lovie-token", tokenAppoggio);

                JObject oJsonObject = new JObject();

                oJsonObject.Add("phone_token", CodeText.Text);

                requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


                IRestResponse response = client.Execute(requestN4U);

                Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    NSUserDefaults.StandardUserDefaults.SetString(tokenAppoggio, "TokenLovieN4U");
                    if(!Registrato)
                    NSUserDefaults.StandardUserDefaults.SetString(tokenfbAppoggio, "TokenLovieFB");

                    UIViewController lp = storyboard.InstantiateViewController("HomePage");
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.PresentViewController(lp, true, null);
                }
                else
                {

                    CodeText.Text = "";

                    var adErr = new UIAlertView("Errore", "Codice non corrispondente Riprovare oppure riavviare l'applicazione per cambiare numero.", null, "Riprova", null);
                    adErr.Show();

                }


            }

        }

        private void ScrollTheView(bool scale)
        {
            UIView.BeginAnimations(string.Empty, IntPtr.Zero);
            UIView.SetAnimationDuration(_animationDuration);
            UIView.SetAnimationCurve(_animationCurve);

            var frame = ContentView.Frame;

            if (scale)
            {
                if (!KeyOpen)
                {
                    frame.Y -= _scrollAmount;
                    KeyOpen = true;
                }
            }
            else
            {
                frame.Y += _scrollAmount;
                KeyOpen = false;
            }
            ContentView.Frame = frame;
            UIView.CommitAnimations();

            
        }

    }
}
