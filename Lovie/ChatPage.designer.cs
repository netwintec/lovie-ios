// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Lovie
{
    [Register ("ChatPage")]
    partial class ChatPage
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        Lovie.NavBarChat navBar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView NotificationView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem revealButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem SegnalaIcon { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (navBar != null) {
                navBar.Dispose ();
                navBar = null;
            }

            if (NotificationView != null) {
                NotificationView.Dispose ();
                NotificationView = null;
            }

            if (revealButton != null) {
                revealButton.Dispose ();
                revealButton = null;
            }

            if (SegnalaIcon != null) {
                SegnalaIcon.Dispose ();
                SegnalaIcon = null;
            }
        }
    }
}