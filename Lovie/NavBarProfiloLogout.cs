using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace Lovie
{
    public partial class NavBarProfiloLogout : UINavigationBar
    {
        public NavBarProfiloLogout (IntPtr handle) : base (handle)
        {
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            //Console.WriteLine(Frame.Width);

            UIImageView centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 35, 4, 70, 36));
            centerlogo.Image = UIImage.FromFile("logo_lovie.png");

            UIImageView back = new UIImageView(new CGRect(15, 12, 20, 20));
            back.Image = UIImage.FromFile("back.png");

            UIImageView logout = new UIImageView(new CGRect(Frame.Width - 35, 12, 20, 20));
            logout.Image = UIImage.FromFile("logout.png");

            AddSubview(centerlogo);
            AddSubview(back);
            AddSubview(logout);
        }

    }
}