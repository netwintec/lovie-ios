// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Lovie
{
	[Register ("ProfilePage")]
	partial class ProfilePage
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel Acquista { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView FifthProduct { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel FifthProductLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel FifthProductPrice { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView FirstProduct { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel FirstProductLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel FirstProductPrice { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView FourthProduct { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel FourthProductLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel FourthProductPrice { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIBarButtonItem LogoutItem { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel Nome { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		NSLayoutConstraint NotificationView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView NotView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView SecondProduct { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel SecondProductLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel SecondProductPrice { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel Telefono { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ThirdProduct { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel ThirdProductLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel ThirdProductPrice { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (Acquista != null) {
				Acquista.Dispose ();
				Acquista = null;
			}
			if (FifthProduct != null) {
				FifthProduct.Dispose ();
				FifthProduct = null;
			}
			if (FifthProductLabel != null) {
				FifthProductLabel.Dispose ();
				FifthProductLabel = null;
			}
			if (FifthProductPrice != null) {
				FifthProductPrice.Dispose ();
				FifthProductPrice = null;
			}
			if (FirstProduct != null) {
				FirstProduct.Dispose ();
				FirstProduct = null;
			}
			if (FirstProductLabel != null) {
				FirstProductLabel.Dispose ();
				FirstProductLabel = null;
			}
			if (FirstProductPrice != null) {
				FirstProductPrice.Dispose ();
				FirstProductPrice = null;
			}
			if (FourthProduct != null) {
				FourthProduct.Dispose ();
				FourthProduct = null;
			}
			if (FourthProductLabel != null) {
				FourthProductLabel.Dispose ();
				FourthProductLabel = null;
			}
			if (FourthProductPrice != null) {
				FourthProductPrice.Dispose ();
				FourthProductPrice = null;
			}
			if (LogoutItem != null) {
				LogoutItem.Dispose ();
				LogoutItem = null;
			}
			if (Nome != null) {
				Nome.Dispose ();
				Nome = null;
			}
			if (NotificationView != null) {
				NotificationView.Dispose ();
				NotificationView = null;
			}
			if (NotView != null) {
				NotView.Dispose ();
				NotView = null;
			}
			if (SecondProduct != null) {
				SecondProduct.Dispose ();
				SecondProduct = null;
			}
			if (SecondProductLabel != null) {
				SecondProductLabel.Dispose ();
				SecondProductLabel = null;
			}
			if (SecondProductPrice != null) {
				SecondProductPrice.Dispose ();
				SecondProductPrice = null;
			}
			if (Telefono != null) {
				Telefono.Dispose ();
				Telefono = null;
			}
			if (ThirdProduct != null) {
				ThirdProduct.Dispose ();
				ThirdProduct = null;
			}
			if (ThirdProductLabel != null) {
				ThirdProductLabel.Dispose ();
				ThirdProductLabel = null;
			}
			if (ThirdProductPrice != null) {
				ThirdProductPrice.Dispose ();
				ThirdProductPrice = null;
			}
		}
	}
}
