using CoreGraphics;
using Foundation;
using RestSharp;
using System;
using System.CodeDom.Compiler;
using System.Text;
using UIKit;

namespace Lovie
{
	partial class RootController : UIViewController
	{

        UIStoryboard storyboard;

        public RootController (IntPtr handle) : base (handle)
		{

		}

        public override void ViewDidLoad()
        {

            base.ViewDidLoad();

            base.NavigationController.NavigationBarHidden = true;

            //UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

            Console.WriteLine("ID:"+UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString());

            if (NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U") == "" && NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U") == null)
            {

                NSUserDefaults.StandardUserDefaults.SetString("", "TokenLovieFB");
                NSUserDefaults.StandardUserDefaults.SetString("", "TokenLovieN4U");

                storyboard = UIStoryboard.FromName("Main", null);
                UIViewController lp = storyboard.InstantiateViewController("LoginPage");
                base.NavigationController.PresentModalViewController(lp, true);

            }
            else
            {

                //DEVELOPMENT
                //var client = new RestClient("http://location.keepup.pro:3000/");
                //DISTRIBUTION
                var client = new RestClient("http://api.netwintec.com:3000/");


                var requestN4U = new RestRequest("api/user/" + NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U"), Method.GET);
                requestN4U.AddHeader("content-type", "application/json");

                var response = client.Execute(requestN4U);

                Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Console.WriteLine("TOKEN:" + NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U"));
                    storyboard = UIStoryboard.FromName("Main", null);
                    UIViewController lp = storyboard.InstantiateViewController("HomePage");
                    base.NavigationController.PresentModalViewController(lp, true);
                }
                else
                {
                    NSUserDefaults.StandardUserDefaults.SetString("", "TokenLovieFB");
                    NSUserDefaults.StandardUserDefaults.SetString("", "TokenLovieN4U");

                    storyboard = UIStoryboard.FromName("Main", null);
                    UIViewController lp = storyboard.InstantiateViewController("LoginPage");
                    base.NavigationController.PresentModalViewController(lp, true);
                }

            }
            /*var fontList = new StringBuilder();
            var familyNames = UIFont.FamilyNames;

            foreach (var familyName in familyNames)
            {
                fontList.Append(String.Format("Family; {0} \n", familyName));
                Console.WriteLine("Family: {0}\n", familyName);

                var fontNames = UIFont.FontNamesForFamilyName(familyName);

                foreach (var fontName in fontNames)
                {
                    Console.WriteLine("\tFont: {0}\n", fontName);
                }

                // Perform any additional setup after loading the view, typically from a nib.
            }*/

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}
