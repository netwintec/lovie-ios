﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lovie
{
    class Message
    {

        public string Text;
        public bool isMine;

        public Message(string text,bool mine)
        {
            Text = text;
            isMine = mine;
        }
    }
}
