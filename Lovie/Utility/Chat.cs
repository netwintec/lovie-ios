﻿using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace Lovie
{
    class Chat
    {

        public string Key,Nome, Messaggio;
		public DateTime DataLastMessage;
        public string profileImage,FacebookId,Number;
        //public Bitmap profileImageBitmap;
        public bool newMessage, isPreliminary;

        public Chat(string key,string nome, string messaggio, DateTime dataLastMessage,bool somethingNew, string image,bool preliminary, string FBId,string numb)
        {
			Key = key;
            Nome = nome;
            Messaggio = messaggio;
            DataLastMessage = dataLastMessage;
			newMessage = somethingNew;
            profileImage = image;
            isPreliminary = preliminary;
            FacebookId = FBId;
            Number = numb;
            //profileImageBitmap = null;

        }


    }

}
