﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace Lovie
{
    class TableSourcePerson : UITableViewSource
    {

        public List<FbPerson> listChat;
        int element;
        int selected;
        ContactPage Super;

        public TableSourcePerson(List<FbPerson> list,ContactPage cp)
        {
            listChat = list;
            element = listChat.Count;
            Super = cp;
            //element = e;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return element;
        }



        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {

            //HomePage.Instance.chatSelected = indexPath.Row;
            ContactPage.Instance.SelectedChat(indexPath.Row);

        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            nfloat height;
            if (indexPath.Row == element-1)
                height = 50;
            else
                height = 62;
            return height;

        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {



            int row = indexPath.Row;
            NSString cellIdentifier = new NSString(row.ToString());

            CustomCellPerson cell = new CustomCellPerson(cellIdentifier); ;

            cell.UpdateCell(listChat[row].Nome, listChat[row].Image_Url);

            return cell;

        }

    }
}

