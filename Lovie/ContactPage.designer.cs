﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Lovie
{
    [Register ("ContactPage")]
    partial class ContactPage
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ButtonContatti { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContainerTabella { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView FBNameButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView FBView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView NameFBCornice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView NameFBInterno { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView NameWACornice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView NameWAInterno { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView NotificationView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView Picker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView WANumberButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView WAView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonContatti != null) {
                ButtonContatti.Dispose ();
                ButtonContatti = null;
            }

            if (ContainerTabella != null) {
                ContainerTabella.Dispose ();
                ContainerTabella = null;
            }

            if (FBNameButton != null) {
                FBNameButton.Dispose ();
                FBNameButton = null;
            }

            if (FBView != null) {
                FBView.Dispose ();
                FBView = null;
            }

            if (NameFBCornice != null) {
                NameFBCornice.Dispose ();
                NameFBCornice = null;
            }

            if (NameFBInterno != null) {
                NameFBInterno.Dispose ();
                NameFBInterno = null;
            }

            if (NameWACornice != null) {
                NameWACornice.Dispose ();
                NameWACornice = null;
            }

            if (NameWAInterno != null) {
                NameWAInterno.Dispose ();
                NameWAInterno = null;
            }

            if (NotificationView != null) {
                NotificationView.Dispose ();
                NotificationView = null;
            }

            if (Picker != null) {
                Picker.Dispose ();
                Picker = null;
            }

            if (WANumberButton != null) {
                WANumberButton.Dispose ();
                WANumberButton = null;
            }

            if (WAView != null) {
                WAView.Dispose ();
                WAView = null;
            }
        }
    }
}