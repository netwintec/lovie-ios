﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UIKit;

namespace Lovie
{
    class CustomCellPerson : UITableViewCell
    {
        UILabel nomeLabel;
        UIImageView imageView;
        UIView separator;
        public CustomCellPerson(NSString cellId) : base(UITableViewCellStyle.Default, cellId)
        {


            SelectionStyle = UITableViewCellSelectionStyle.Default;
            //ContentView.BackgroundColor = UIColor.FromRGB(218, 255, 127);
            imageView = new UIImageView();

            separator = new UIView();
            separator.BackgroundColor = UIColor.FromRGB(76, 156, 206);

            nomeLabel = new UILabel()
            {
                Font = UIFont.FromName("Roboto-Regular", 15f),
                TextColor = UIColor.FromRGB(0, 0, 0),
                TextAlignment = UITextAlignment.Left,
                BackgroundColor = UIColor.Clear

            };

            //ContentView.BackgroundColor = UIColor.FromRGB(230,230,230);
            ContentView.AddSubviews(new UIView[] { nomeLabel, imageView, separator });

        }
        public void UpdateCell(string nome, string urlImage)
        {
            imageView.Image = UIImage.FromFile("profilePicture.png");
            nomeLabel.Text = nome;
            SetImageAsync(imageView, urlImage);
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            nfloat widht = ContentView.Frame.Width;
            nfloat height = ContentView.Frame.Height;

            imageView.Frame = new CGRect(15, 5, 50, 50);
            imageView.Layer.CornerRadius = 25f;
            imageView.Layer.MasksToBounds = true;
            nomeLabel.Frame = new CGRect(80, 11, widht - 95, 20);
            separator.Frame = new CGRect(15, 60, widht - 30, 2);
        }

        public void SetImageAsync(UIImageView image, string url)
        {
            UIImage Placeholder = UIImage.FromFile("profilePicture.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(url) ?? Placeholder;

                    InvokeOnMainThread(() =>
                    {

                        image.Image = cachedImage;

                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }


        static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                if (data != null)
                    return UIImage.LoadFromData(data);
            return null;
        }
    }
}
