using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Lovie
{
	partial class NavBarChat : UINavigationBar
	{

        UIImageView reveal;

        public NavBarChat (IntPtr handle) : base (handle)
		{
		}

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            //Console.WriteLine(Frame.Width);

            UIImageView centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 35, 4, 70, 36));
            centerlogo.Image = UIImage.FromFile("logo_lovie.png");

            UIImageView back = new UIImageView(new CGRect(15, 9, 26, 26));
            back.Image = UIImage.FromFile("back.png");

            reveal = new UIImageView(new CGRect(Frame.Width-38, 9, 23, 26));
            reveal.Image = UIImage.FromFile("alpha.png");

            UIImageView segnala = new UIImageView(new CGRect(Frame.Width - 76, 12, 20, 20));
            segnala.Image = UIImage.FromFile("segnala.png");

            AddSubview(centerlogo);
            AddSubview(reveal);
            AddSubview(segnala);
            AddSubview(back);
        }

        public void SeeReveal() {
            reveal.Image = UIImage.FromFile("reveal.png");
        }

        public void RemoveReveal()
        {
            reveal.Image = UIImage.FromFile("alpha.png");
        }
    }
}
