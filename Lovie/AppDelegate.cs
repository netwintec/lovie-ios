﻿using Foundation;
using UIKit;
using Facebook.CoreKit;
using System;
using Xamarin.InAppPurchase;
using Xamarin.InAppPurchase.Utilities;

namespace Lovie
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        // class-level declarations

        public string APNToken="";

        public bool isHome = false;
        public bool isChat = false;
        public bool isContact = false;
        public bool isProfile = false;
        public string Key = "";

        bool Background = false;

        public InAppPurchaseManager PurchaseManager = new InAppPurchaseManager();
        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            // We need to handle URLs by passing them to their own OpenUrl in order to make the SSO authentication works.
            return ApplicationDelegate.SharedInstance.OpenUrl(application, url, sourceApplication, annotation);
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method

            AppDelegate.Instance = this;

            //IN-APP PURCHASE
            //c45ecb0b3 dfb45fc9f5b 1cbfc9c 2277f
            string value = Xamarin.InAppPurchase.Utilities.Security.Unify(
                new string[] { "dfb45fc9f5b",
                    "1cbfc9c",
                    "c45ecb0b3",
                    "2277f" },
                new int[] { 2, 0, 1, 3 });



            PurchaseManager.SimulateiTunesAppStore = false;

            //PurchaseManager.SimulateiTunesAppStore = false;

            PurchaseManager.PublicKey = value;
            PurchaseManager.ApplicationUserName = "Lovie";

            // Warn user that the store is not available
            if (PurchaseManager.CanMakePayments)
            {
                Console.WriteLine("Xamarin.InAppBilling: User can make payments to iTunes App Store.");
            }
            else
            {
                //Display Alert Dialog Box
                using (var alert = new UIAlertView("Xamarin.InAppBilling", "Sorry but you cannot make purchases from the In App Billing store. Please try again later.", null, "OK", null))
                {
                    alert.Show();
                }

            }
            // Warn user if the Purchase Manager is unable to connect to
            // the network.
            PurchaseManager.NoInternetConnectionAvailable += () => {
                //Display Alert Dialog Box
                using (var alert = new UIAlertView("Xamarin.InAppBilling", "No open internet connection is available.", null, "OK", null))
                {
                    alert.Show();
                }
            };

            // Show any invalid product queries
            PurchaseManager.ReceivedInvalidProducts += (productIDs) => {
                // Display any invalid product IDs to the console
                Console.WriteLine("The following IDs were rejected by the iTunes App Store:");
                foreach (string ID in productIDs)
                {
                    Console.WriteLine(ID);
                }
                Console.WriteLine(" ");
            };

            // Report the results of the user restoring previous purchases
            PurchaseManager.InAppPurchasesRestored += (count) => {
                // Anything restored?
                if (count == 0)
                {
                    // No, inform user
                    using (var alert = new UIAlertView("Xamarin.InAppPurchase", "No products were available to be restored from the iTunes App Store.", null, "OK", null))
                    {
                        alert.Show();
                    }
                }
                else
                {
                    // Yes, inform user
                    using (var alert = new UIAlertView("Xamarin.InAppPurchase", String.Format("{0} {1} restored from the iTunes App Store.", count, (count > 1) ? "products were" : "product was"), null, "OK", null))
                    {
                        alert.Show();
                    }
                }
            };

            // Report miscellanous processing errors
            PurchaseManager.InAppPurchaseProcessingError += (message) => {
                //Display Alert Dialog Box
                using (var alert = new UIAlertView("Xamarin.InAppPurchase", message, null, "OK", null))
                {
                    alert.Show();
                }
            };

            // Report any issues with persistence
            PurchaseManager.InAppProductPersistenceError += (message) => {
                using (var alert = new UIAlertView("Xamarin.InAppPurchase", message, null, "OK", null))
                {
                    alert.Show();
                }
            };

            // Setup automatic purchase persistance and load any previous purchases
            PurchaseManager.AutomaticPersistenceType = InAppPurchasePersistenceType.LocalFile;
            PurchaseManager.PersistenceFilename = "LovieData";
            PurchaseManager.ShuffleProductsOnPersistence = false;
            PurchaseManager.RestoreProducts();

            
			// Ask the iTunes App Store to return information about available In App Products for sale
			PurchaseManager.QueryInventory (new string[] {
                "com.lastlime.lovie.message.consumable_x10",
                "com.lastlime.lovie.message.consumable_x50",
                "com.lastlime.lovie.message.consumable_x200",
                "com.lastlime.lovie.message.consumable_x500",
                "com.lastlime.lovie.message.consumable_x1000"
            });

            //NOTIFICATION
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }

            if (launchOptions != null && launchOptions.Keys != null && launchOptions.Keys.Length != 0 && launchOptions.ContainsKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")))
            {
                Console.WriteLine("entro");
                NSDictionary userInfo = launchOptions.ObjectForKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")) as NSDictionary;
                //UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                Console.WriteLine("spenta");
                string message = string.Empty;
                string conversation_id = string.Empty;
                string name = string.Empty;

                /*foreach( var obj in userInfo)
                {
                    Console.WriteLine(obj.Key + ":" + obj.Value);
                }*/

                if (userInfo.ContainsKey(new NSString("message")))
                    message = (userInfo[new NSString("message")] as NSString).ToString();
                if (userInfo.ContainsKey(new NSString("conversation_id")))
                    conversation_id = (userInfo[new NSString("conversation_id")] as NSString).ToString();
                if (userInfo.ContainsKey(new NSString("sender_name")))
                    name = (userInfo[new NSString("sender_name")] as NSString).ToString();

                NSUserDefaults.StandardUserDefaults.SetString(name, "NameLovieNotification");
                NSUserDefaults.StandardUserDefaults.SetString(conversation_id, "KeyLovieNotification");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "isPushLovieNotification");
                Console.WriteLine("PUSH:"+ NSUserDefaults.StandardUserDefaults.BoolForKey("isPushLovieNotification"));
                //ProcessNotification(UIApplicationLaunchOptionsRemoteNotificationKey);
            }

            return true;
        }

        public static AppDelegate Instance { get; private set; }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {

            //Get current device token
            var DeviceToken = deviceToken.Description;
            Console.WriteLine("PUSH TOKEN1:" + DeviceToken);
            if (!string.IsNullOrWhiteSpace(DeviceToken))
            {
                DeviceToken = DeviceToken.Replace("<", "").Replace(">", "").Replace(" ", "");
            }

            APNToken = DeviceToken;

        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            new UIAlertView("Error registering push notifications", error.LocalizedDescription, null, "OK", null).Show();
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            //This method gets called whenever the app is already running and receives a push notification
            // YOU MUST HANDLE the notifications in this case.  Apple assumes if the app is running, it takes care of everything
            // this includes setting the badge, playing a sound, etc.

            Console.WriteLine("remote" + application.ApplicationState.ToString());
            

            if (application.ApplicationState == UIApplicationState.Inactive || application.ApplicationState == UIApplicationState.Background)
            {


                    Console.WriteLine("back");

                    //UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                    string message = string.Empty;
                    string conversation_id = string.Empty;
                    string name = string.Empty;

                    /*foreach( var obj in userInfo)
                    {
                        Console.WriteLine(obj.Key + ":" + obj.Value);
                    }*/

                    if (userInfo.ContainsKey(new NSString("message")))
                        message = (userInfo[new NSString("message")] as NSString).ToString();
                    if (userInfo.ContainsKey(new NSString("conversation_id")))
                        conversation_id = (userInfo[new NSString("conversation_id")] as NSString).ToString();
                    if (userInfo.ContainsKey(new NSString("sender_name")))
                        name = (userInfo[new NSString("sender_name")] as NSString).ToString();

                    Console.WriteLine("object:" + message + "|" + conversation_id);

                    HomePage.Instance.Key = conversation_id;
                    HomePage.Instance.name = name;
                    HomePage.Instance.FacebookId = "";
                    HomePage.Instance.isPreliminary = false;
                    HomePage.Instance.isFirstMessage = false;
                    HomePage.Instance.number = "";

                    NSUserDefaults.StandardUserDefaults.SetBool(true, "isPushLovieNotification");
                    NSUserDefaults.StandardUserDefaults.SetString(name, "NameLovieNotification");
                    NSUserDefaults.StandardUserDefaults.SetString(conversation_id, "KeyLovieNotification");

                    UIStoryboard storyboard = new UIStoryboard();
                    storyboard = UIStoryboard.FromName("Main", null);

                    UIViewController lp = storyboard.InstantiateViewController("ChatPage");
                    //application.Delegate.GetWindow().RootViewController.PresentModalViewController(lp, true);
                    TopController().PresentModalViewController(lp, true);
                //}

            }
            if (application.ApplicationState == UIApplicationState.Active)
            {
                //UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                string message = string.Empty;
                string conversation_id = string.Empty;


                foreach( var obj in userInfo)
                {
                    Console.WriteLine(obj.Key + ":" + obj.Value);
                }

                if (userInfo.ContainsKey(new NSString("message")))
                    message = (userInfo[new NSString("message")] as NSString).ToString();
                if (userInfo.ContainsKey(new NSString("conversation_id")))
                    conversation_id = (userInfo[new NSString("conversation_id")] as NSString).ToString();

                Console.WriteLine("object:" + message + "|" + conversation_id);

                if (isHome)
                {

                    HomePage.Instance.ChangeDataNotification();
                    
                }

                if (isChat)
                {

                    if (conversation_id == Key)
                    {
                        ChatPage.Instance.AddNewMessage(message);
                    }
                    else
                    {
                        ChatPage.Instance.CompareNotification();
                    }

                }

                if (isContact)
                {

                    ContactPage.Instance.CompareNotification();

                }

                if (isProfile)
                {

                    ProfilePage.Instance.CompareNotification();

                }

            }
        }

        public void ProcessNotification(NSDictionary userInfo)
        {
            //UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

            Console.WriteLine("spenta");
            string message = string.Empty;
            string conversation_id = string.Empty;
            string name = string.Empty;

                        /*foreach( var obj in userInfo)
                        {
                            Console.WriteLine(obj.Key + ":" + obj.Value);
                        }*/

            if (userInfo.ContainsKey(new NSString("message")))
                message = (userInfo[new NSString("message")] as NSString).ToString();
            if (userInfo.ContainsKey(new NSString("conversation_id")))
                conversation_id = (userInfo[new NSString("conversation_id")] as NSString).ToString();
            if (userInfo.ContainsKey(new NSString("sender_name")))
                name = (userInfo[new NSString("sender_name")] as NSString).ToString();

            NSUserDefaults.StandardUserDefaults.SetString(name, "NameLovieNotification");
            NSUserDefaults.StandardUserDefaults.SetString(conversation_id, "KeyLovieNotification");
            NSUserDefaults.StandardUserDefaults.SetBool(true, "isPushLovieNotification");
                    
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            Console.WriteLine("Back:" + NSUserDefaults.StandardUserDefaults.BoolForKey("TokenLovieN4U"));
            Background = true;
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.

            if (isHome)
            {

                HomePage.Instance.ChangeDataNotification();

            }
            if (isChat)
            {

                ChatPage.Instance.ReinizializePage();

            }

        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            Background = false;
        }

        public UIViewController TopController()
        {
            UIViewController topController = (UIApplication.SharedApplication).KeyWindow.RootViewController;
            while (topController.PresentedViewController != null)
            {
                topController = topController.PresentedViewController;
            }

            return topController;
        }
    }
}
 