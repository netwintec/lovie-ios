﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace Lovie
{
    class CustomCellMessageSend : UITableViewCell
    {
        UILabel messaggioLabel;
        UIImageView imageView;
        UIView bubble, rectangle;

        float TextWidht, TextHeight;

        public CustomCellMessageSend(NSString cellId, float widht, float height) : base(UITableViewCellStyle.Default, cellId)
        {

            TextWidht = widht;
            TextHeight = height;

            SelectionStyle = UITableViewCellSelectionStyle.None;
            //ContentView.BackgroundColor = UIColor.FromRGB(218, 255, 127);
            imageView = new UIImageView();
            bubble = new UIView();
            bubble.BackgroundColor = UIColor.FromRGB(87, 196, 241);
            bubble.Layer.CornerRadius = 7;

            rectangle = new UIView();
            rectangle.BackgroundColor = UIColor.FromRGB(87, 196, 241);

            messaggioLabel = new UILabel()
            {
                Font = UIFont.FromName("Roboto-Regular", 15f),
                TextColor = UIColor.FromRGB(255, 255, 255),
                Lines = 0,
                TextAlignment = UITextAlignment.Left,
                BackgroundColor = UIColor.Clear

            };

            bubble.Add(messaggioLabel);

            ContentView.AddSubviews(new UIView[] { rectangle, bubble, imageView });

        }
        public void UpdateCell(string messaggio, UIImage image)
        {
            imageView.Image = image;
            messaggioLabel.Text = messaggio;
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            nfloat widht = ContentView.Frame.Width;
            nfloat height = ContentView.Frame.Height;

            messaggioLabel.Frame = new CGRect(10, 10, TextWidht, TextHeight);

            imageView.Frame = new CGRect(widht -30, height - 25, 15, 15);
            rectangle.Frame = new CGRect(widht - 45, height - 22, 12, 12);
            bubble.Frame = new CGRect(widht - 33 - (TextWidht + 20), 10, TextWidht + 20, TextHeight + 20);

        }
    }
}
