﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace Lovie
{
    class TableSourceChat : UITableViewSource
    {

        public List<Message> listMessage;
        public int element;
        float Widht;
        //int selected;
        ChatPage super;

        public TableSourceChat(List<Message> list,float w,ChatPage cp)
        {
            listMessage = list;
            Widht = w;
            element = listMessage.Count+1;
            super = cp;
            //element = e;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return element;
        }



        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            Console.WriteLine(2);
            super.ToggleKeyboard();
            /*base.RowSelected(tableView, indexPath);

            if (indexPath.Row == selected)
            {
                selected = ListAquistiObject.Count + 10;
                righe = ListAquistiObject.Count;
                descAp = false;
            }
            else
            {
                if (indexPath.Row < selected)
                {
                    selected = indexPath.Row;
                    righe = ListAquistiObject.Count + 1;
                    descAp = true;
                }
                if (indexPath.Row > selected + 1)
                {
                    selected = indexPath.Row - 1;
                    righe = ListAquistiObject.Count + 1;
                    descAp = true;
                }
            }

            tableView.ReloadData();*/
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            

            int row = indexPath.Row;
            if (row == 0)
            {
                return 40;
            }
            else
            {
                string text = listMessage[row-1].Text;

                var size = UIStringDrawing.StringSize(text, UIFont.FromName("Roboto-Regular", 15), new CGSize(Widht - 110, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);

                nfloat height = size.Height + 40;
                return height;
            }
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            Console.WriteLine(element);

            int row = indexPath.Row;
            if (indexPath.Row == 0)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                return new UITableViewCell(UITableViewCellStyle.Default, cellIdentifier);
            }
            else
            { 
                Console.WriteLine(row);

                string text = listMessage[row-1].Text;

                var size = UIStringDrawing.StringSize(text, UIFont.FromName("Roboto-Regular", 15), new CGSize(Widht - 110, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);

                if (!listMessage[row-1].isMine)
                {
                    NSString cellIdentifier = new NSString(row.ToString());
                    CustomCellMessageReceive cell = new CustomCellMessageReceive(cellIdentifier, (float)size.Width, (float)size.Height);

                    cell.UpdateCell(listMessage[row-1].Text, UIImage.FromFile("cuore_ricevi.png"));
                    return cell;
                }
                else
                {
                    NSString cellIdentifier = new NSString(row.ToString());
                    CustomCellMessageSend cell = new CustomCellMessageSend(cellIdentifier, (float)size.Width, (float)size.Height);

                    cell.UpdateCell(listMessage[row-1].Text, UIImage.FromFile("cuore_invio.png"));
                    return cell;
                }
            }

        }

    }
}
