// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Lovie
{
    [Register ("LoginPage")]
    partial class LoginPage
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView avanti { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView BottomCheckView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView BottomFBView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView BottomLoadView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ChiudiButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ChiudiView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CodeText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView CodeView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Cognome { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContentView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView FBView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIActivityIndicatorView LoadAnim { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Nome { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PhoneText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PhoneView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PopUpView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PrefixText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SendButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView TermView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView TextView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (avanti != null) {
                avanti.Dispose ();
                avanti = null;
            }

            if (BottomCheckView != null) {
                BottomCheckView.Dispose ();
                BottomCheckView = null;
            }

            if (BottomFBView != null) {
                BottomFBView.Dispose ();
                BottomFBView = null;
            }

            if (BottomLoadView != null) {
                BottomLoadView.Dispose ();
                BottomLoadView = null;
            }

            if (ChiudiButton != null) {
                ChiudiButton.Dispose ();
                ChiudiButton = null;
            }

            if (ChiudiView != null) {
                ChiudiView.Dispose ();
                ChiudiView = null;
            }

            if (CodeText != null) {
                CodeText.Dispose ();
                CodeText = null;
            }

            if (CodeView != null) {
                CodeView.Dispose ();
                CodeView = null;
            }

            if (Cognome != null) {
                Cognome.Dispose ();
                Cognome = null;
            }

            if (ContentView != null) {
                ContentView.Dispose ();
                ContentView = null;
            }

            if (FBView != null) {
                FBView.Dispose ();
                FBView = null;
            }

            if (LoadAnim != null) {
                LoadAnim.Dispose ();
                LoadAnim = null;
            }

            if (Nome != null) {
                Nome.Dispose ();
                Nome = null;
            }

            if (PhoneText != null) {
                PhoneText.Dispose ();
                PhoneText = null;
            }

            if (PhoneView != null) {
                PhoneView.Dispose ();
                PhoneView = null;
            }

            if (PopUpView != null) {
                PopUpView.Dispose ();
                PopUpView = null;
            }

            if (PrefixText != null) {
                PrefixText.Dispose ();
                PrefixText = null;
            }

            if (SendButton != null) {
                SendButton.Dispose ();
                SendButton = null;
            }

            if (TermView != null) {
                TermView.Dispose ();
                TermView = null;
            }

            if (TextView != null) {
                TextView.Dispose ();
                TextView = null;
            }
        }
    }
}