﻿using System;
using System.Collections.Generic;
using System.Text;
using Foundation;
using UIKit;

namespace Lovie
{
    partial class TableSourceHome : UITableViewSource
    {

        public List<Chat> listChat;
        public int element;
        int selected;


        public TableSourceHome(List<Chat> list)
        {
            listChat = list;
            element = listChat.Count;
            //element = e;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return element;
        }



        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {

            HomePage.Instance.chatSelected = indexPath.Row;
            HomePage.Instance.SelectedChat(indexPath.Row);

        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            nfloat height = 75;
            return height;

        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            

            int row = indexPath.Row;
            NSString cellIdentifier = new NSString(row.ToString());

            CustomCellChat cell;

            if (listChat[row].newMessage) {
                cell = new CustomCellChat(cellIdentifier, false);
            }
            else
            {
                cell = new CustomCellChat(cellIdentifier, true);
            }

            DateTime now = DateTime.Now.ToLocalTime();

            Console.WriteLine(listChat[row].Nome + "|" + listChat[row].Messaggio + "|" + listChat[row].DataLastMessage.ToString("HH':'mm") + "|" + listChat[row].profileImage + "|" + listChat[row].newMessage);
            if (listChat[row].DataLastMessage.Month == now.Month && listChat[row].DataLastMessage.Day == now.Day && listChat[row].DataLastMessage.Year == now.Year)
            {
                cell.UpdateCell(listChat[row].Nome, listChat[row].Messaggio, listChat[row].DataLastMessage.ToString("HH':'mm"), listChat[row].profileImage);
            }
            else
            {
                cell.UpdateCell(listChat[row].Nome, listChat[row].Messaggio, listChat[row].DataLastMessage.ToString("dd'/'MM"), listChat[row].profileImage);
            }
            return cell;

        }

    }
}
