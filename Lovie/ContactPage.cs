﻿using AddressBookUI;
using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Collections.Generic;
using System.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using CoreAnimation;
using ContactsUI;
using Contacts;

namespace Lovie
{
	partial class ContactPage : UIViewController
	{

        public UIStoryboard storyboard = UIStoryboard.FromName("Main", null);
        bool isFacebook = true;
        bool keyboardOpen = false;
        bool PlaceholderFB = true;
        bool PlaceholderWA = true;
        bool TabellaOpen = false;
        UITextView FBName,WANumber;
        ABPeoplePickerNavigationController _contactController;
        CNContactPickerViewController _contactController2;
        UIView CorniceTabella;
        UITableView tabellaFb;
        List<FbPerson> ListPeople = new List<FbPerson>();
        TableSourcePerson Person;
        public string prefix = "+39";

        public ContactPage (IntPtr handle) : base (handle)
		{
		}

        public static ContactPage Instance { get; private set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ContactPage.Instance = this;

            AppDelegate.Instance.isHome = false;
            AppDelegate.Instance.isChat = false;
            AppDelegate.Instance.isProfile = false;
            AppDelegate.Instance.isContact = true;

            UIView UpperView = new UIView(new CGRect(0, 69, View.Frame.Width, 25));
            UpperView.BackgroundColor = UIColor.FromRGB(220,220,220);

            UIView FBCornice = new UIView(new CGRect(-2, 84, (View.Frame.Width/2)+3, 54));
            FBCornice.Layer.CornerRadius = 10;
            FBCornice.BackgroundColor = UIColor.FromRGB(76, 156, 206);

            UIView WACornice = new UIView(new CGRect((View.Frame.Width/2)-1, 84, (View.Frame.Width / 2) + 3, 54));
            WACornice.Layer.CornerRadius = 10;
            WACornice.Alpha = 0;
            WACornice.BackgroundColor = UIColor.FromRGB(76, 156, 206);

            UIView CentralSeparator = new UIView(new CGRect((View.Frame.Width / 2) - 1, 130, 2, 8));
            CentralSeparator.BackgroundColor = UIColor.FromRGB(76, 156, 206);

            UIView LeftSeparator = new UIView(new CGRect(0, 136, View.Frame.Width / 2 - 1, 2));
            LeftSeparator.BackgroundColor = UIColor.White;

            UIView RightSeparator = new UIView(new CGRect(View.Frame.Width / 2 + 1, 136, View.Frame.Width / 2 - 1, 2));
            RightSeparator.BackgroundColor = UIColor.FromRGB(76, 156, 206);

            UIView FBBackButton = new UIView(new CGRect(0, 96, View.Frame.Width / 2 - 1, 42));
            FBBackButton.BackgroundColor = UIColor.White;

            UIView WABackButton = new UIView(new CGRect(View.Frame.Width / 2 + 1, 96, View.Frame.Width / 2 - 1, 42));
            WABackButton.BackgroundColor = UIColor.FromRGB(220, 220, 220);

            UIView FBButton = new UIView(new CGRect(0, 86, View.Frame.Width / 2 - 1, 52));
            FBButton.Layer.CornerRadius = 8;
            FBButton.BackgroundColor = UIColor.White;

            UIView WAButton = new UIView(new CGRect(View.Frame.Width / 2 + 1, 86, View.Frame.Width / 2 - 1, 52));
            WAButton.Layer.CornerRadius = 8;
            WAButton.BackgroundColor = UIColor.FromRGB(220, 220, 220);

            UITapGestureRecognizer FBButtonTap = new UITapGestureRecognizer(() =>
            {

                if (keyboardOpen)
                {
                    if (isFacebook)
                    {
                        FBName.ResignFirstResponder();
                    }
                    else
                    {
                        WANumber.ResignFirstResponder();
                    }
                }

                if (!isFacebook)
                {
                    FBCornice.Alpha = 1;
                    WACornice.Alpha = 0;
                    LeftSeparator.BackgroundColor = UIColor.White;
                    RightSeparator.BackgroundColor = UIColor.FromRGB(76, 156, 206);
                    isFacebook = true;
                    FBView.Alpha = 1;
                    WAView.Alpha = 0;
                    FBButton.BackgroundColor = UIColor.White;
                    WAButton.BackgroundColor = UIColor.FromRGB(220, 220, 220);
                    FBBackButton.BackgroundColor = UIColor.White;
                    WABackButton.BackgroundColor = UIColor.FromRGB(220, 220, 220);

                }

            });
            FBButtonTap.CancelsTouchesInView = false;
            FBButton.UserInteractionEnabled = true;
            FBButton.AddGestureRecognizer(FBButtonTap);

            UITapGestureRecognizer WAButtonTap = new UITapGestureRecognizer(() =>
            {

                if (keyboardOpen)
                {
                    if (isFacebook)
                    {
                        FBName.ResignFirstResponder();
                    }
                    else
                    {
                        WANumber.ResignFirstResponder();
                    }
                }

                if (isFacebook)
                {
                    FBCornice.Alpha = 0;
                    WACornice.Alpha = 1;
                    RightSeparator.BackgroundColor = UIColor.White;
                    LeftSeparator.BackgroundColor = UIColor.FromRGB(76, 156, 206);
                    isFacebook = false;
                    FBView.Alpha = 0;
                    WAView.Alpha = 1;
                    FBButton.BackgroundColor = UIColor.FromRGB(220, 220, 220);
                    WAButton.BackgroundColor = UIColor.White;
                    FBBackButton.BackgroundColor = UIColor.FromRGB(220, 220, 220);
                    WABackButton.BackgroundColor = UIColor.White;

                }

            });
            WAButtonTap.CancelsTouchesInView = false;
            WAButton.UserInteractionEnabled = true;
            WAButton.AddGestureRecognizer(WAButtonTap);

            UIImageView FBChat = new UIImageView(new CGRect(FBButton.Frame.Width / 2 - 20, 6, 39, 39));
            FBChat.Image = UIImage.FromFile("FBChat.png");

            UIImageView WAChat = new UIImageView(new CGRect(WAButton.Frame.Width / 2 - 20, 6, 39, 39));
            WAChat.Image = UIImage.FromFile("WAChat.png");


            FBButton.Add(FBChat);
            WAButton.Add(WAChat);

            View.Add(UpperView);
            View.Add(FBCornice);
            View.Add(WACornice);
            View.Add(FBBackButton);
            View.Add(WABackButton);
            View.Add(FBButton);
            View.Add(WAButton);
            View.Add(CentralSeparator);
            View.Add(RightSeparator);
            View.Add(LeftSeparator);

            // *********************** FB Part **************************

            NameFBCornice.Layer.CornerRadius = 7;
            NameFBInterno.Layer.CornerRadius = 7;

            UITapGestureRecognizer FBViewTap = new UITapGestureRecognizer(() =>
            {
                if (keyboardOpen)
                {
                        FBName.ResignFirstResponder();
                }

            });
            FBViewTap.CancelsTouchesInView = false;
            FBView.UserInteractionEnabled = true;
            FBView.AddGestureRecognizer(FBViewTap);

            var size = UIStringDrawing.StringSize("Inserisci il nome", UIFont.FromName("Roboto-Regular", 16), new CGSize(NameFBInterno.Frame.Width - 55, 2000));
            Console.WriteLine(size.Height + "  " + size.Width+"    "+ (NameFBInterno.Frame.Height / 2 - (size.Height / 2)));

            FBName = new UITextView(new CGRect(5, NameFBInterno.Frame.Height/2 -(size.Height/2), NameFBInterno.Frame.Width - 55, size.Height));
            FBName.KeyboardType = UIKeyboardType.Default;
            FBName.BackgroundColor = UIColor.Clear;
            FBName.TextColor = UIColor.FromRGB(100, 100, 100);
            FBName.TextContainerInset = new UIEdgeInsets(0, 0, 0, 0);
            FBName.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            FBName.Font = UIFont.FromName("Roboto-Regular", 16);
            FBName.Text = "Inserisci il nome";

            FBName.ShouldBeginEditing += (UITextField) => {
                if (FBName.Text == "Inserisci il nome")
                {
                    FBName.Text = "";
                    FBName.TextColor = UIColor.Black;
                }
                keyboardOpen = true;
                PlaceholderFB = false;

                if (TabellaOpen) {
                    tabellaFb.RemoveFromSuperview() ;
                    CorniceTabella.RemoveFromSuperview();
                    TabellaOpen = false;
                    }
                return true;
            };

            FBName.ShouldEndEditing += (UITextField) => {
                if (string.IsNullOrEmpty(FBName.Text) || string.IsNullOrWhiteSpace(FBName.Text))
                {
                    FBName.Text = "Inserisci il nome";
                    PlaceholderFB = true;
                    FBName.TextColor = UIColor.FromRGB(100, 100, 100);
                }
                keyboardOpen = false;
                return true;
            };

            UITapGestureRecognizer SendFBMessage = new UITapGestureRecognizer(() =>
            {
                if(!string.IsNullOrEmpty(FBName.Text) && !string.IsNullOrWhiteSpace(FBName.Text) && !PlaceholderFB && !TabellaOpen)
                {
                    ListPeople.Clear();

                    RetriveFBPerson(FBName.Text);
                    /*
                    ListPeople.Add(new FbPerson("Mario","Rossi", "http://www.netwintec.com/wp-content/uploads/2014/03/full03-1024x640.jpg"));
                    ListPeople.Add(new FbPerson("Mario", "Bianchi", "http://www.netwintec.com/wp-content/uploads/2014/03/full03-1024x640.jpg"));
                    ListPeople.Add(new FbPerson("Mario", "aaa", "http://www.netwintec.com/wp-content/uploads/2014/03/full03-1024x640.jpg"));
                    ListPeople.Add(new FbPerson("Mario", "bbb", "http://www.netwintec.com/wp-content/uploads/2014/03/full03-1024x640.jpg"));
                    ListPeople.Add(new FbPerson("Mario", "ccc", "http://www.netwintec.com/wp-content/uploads/2014/03/full03-1024x640.jpg"));
                    */

                    var tableH = (42 * ListPeople.Count) + 4 + 8;
                    if (ListPeople.Count > 0) {
                        if (tableH > ContainerTabella.Frame.Height)
                            CorniceTabella = new UIView(new CGRect(0, 0, ContainerTabella.Frame.Width, ContainerTabella.Frame.Height));
                        else
                            CorniceTabella = new UIView(new CGRect(0, 0, ContainerTabella.Frame.Width, (42 * ListPeople.Count) + 4 + 8));
                    }
                    CorniceTabella.BackgroundColor = UIColor.White;//FromRGB(80,80,80);

                    Person = new TableSourcePerson(ListPeople, this);

                    tabellaFb = new UITableView(new CGRect(2,2,CorniceTabella.Frame.Width-4, CorniceTabella.Frame.Height - 4));
                    tabellaFb.BackgroundColor = UIColor.White;//FromRGB(230, 230, 230);
                    tabellaFb.SeparatorStyle = UITableViewCellSeparatorStyle.None;
                    tabellaFb.Source = Person;
                    tabellaFb.SeparatorColor = UIColor.Clear;
                    //tabellaFb.AllowsSelection = false;

                    CorniceTabella.Add(tabellaFb);
                    ContainerTabella.Add(CorniceTabella);

                    TabellaOpen = true;

                }

                if (keyboardOpen)
                {
                    FBName.ResignFirstResponder();
                }

                Console.WriteLine("Send FB");
            });
            SendFBMessage.CancelsTouchesInView = false;
            FBNameButton.UserInteractionEnabled = true;
            FBNameButton.AddGestureRecognizer(SendFBMessage);

            NameFBInterno.Add(FBName);

            // *********************** WA Part **************************
       
            NameWACornice.Layer.CornerRadius = 7;
            NameWAInterno.Layer.CornerRadius = 7;

            UITapGestureRecognizer WAViewTap = new UITapGestureRecognizer(() =>
            {
                if (keyboardOpen)
                {
                    WANumber.ResignFirstResponder();
                }

            });
            WAViewTap.CancelsTouchesInView = false;
            WAView.UserInteractionEnabled = true;
            WAView.AddGestureRecognizer(WAViewTap);

            PickerModel model = new PickerModel(new List<string>() { "+39", "+34", "+33", "+49", "+1", "+44", "+41", "+32", "+31", "+420", "+351", "+385", "+386", "+353" });


            Picker.Layer.CornerRadius = 2;
            Picker.ShowSelectionIndicator = true;
            Picker.Model = model;
            Picker.Hidden = false;

            size = UIStringDrawing.StringSize("Scrivi il numero", UIFont.FromName("Roboto-Regular", 14), new CGSize(NameWAInterno.Frame.Width - 55, 2000));
            Console.WriteLine(size.Height + "  " + size.Width + "    " + (NameWAInterno.Frame.Height / 2 - (size.Height / 2)));

            WANumber = new UITextView(new CGRect(5, NameWAInterno.Frame.Height / 2 - (size.Height / 2), NameWAInterno.Frame.Width - 55, size.Height));
            WANumber.KeyboardType = UIKeyboardType.Default;
            WANumber.BackgroundColor = UIColor.Clear;
            WANumber.TextColor = UIColor.FromRGB(100, 100, 100);
            WANumber.TextContainerInset = new UIEdgeInsets(0, 0, 0, 0);
            WANumber.ContentInset = new UIEdgeInsets(0, 0, 0, 0);
            WANumber.Font = UIFont.FromName("Roboto-Regular", 14);
            WANumber.Text = "Scrivi il numero";
            WANumber.KeyboardType = UIKeyboardType.NumberPad;

            WANumber.ShouldBeginEditing += (UITextField) => {
                if (WANumber.Text == "Scrivi il numero")
                {
                    WANumber.Text = "";
                    WANumber.TextColor = UIColor.Black;
                }
                keyboardOpen = true;
                PlaceholderWA = false;
                return true;
            };

            WANumber.ShouldEndEditing += (UITextField) => {
                if (string.IsNullOrEmpty(WANumber.Text) || string.IsNullOrWhiteSpace(WANumber.Text))
                {
                    WANumber.Text = "Scrivi il numero";
                    PlaceholderWA = true;
                    WANumber.TextColor = UIColor.FromRGB(100, 100, 100);
                }
                keyboardOpen = false;
                return true;
            };

            UITapGestureRecognizer SendWAMessage = new UITapGestureRecognizer(() =>
            {
                if (!string.IsNullOrEmpty(WANumber.Text) && !string.IsNullOrWhiteSpace(WANumber.Text) && !PlaceholderWA)
                {
                    if (keyboardOpen)
                    {
                        WANumber.ResignFirstResponder();
                    }
                    Console.WriteLine("Send WA");
                    HomePage.Instance.name = WANumber.Text;
                    HomePage.Instance.isFirstMessage = true;
                    HomePage.Instance.isPreliminary = true;
                    HomePage.Instance.number = prefix+WANumber.Text;



                    UIViewController lp = storyboard.InstantiateViewController("ChatPage");
                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                    this.PresentViewController(lp, true, null);
                }
                //this.PresentModalViewController(_contactController, true);
            });
            SendWAMessage.CancelsTouchesInView = false;
            WANumberButton.UserInteractionEnabled = true;
            WANumberButton.AddGestureRecognizer(SendWAMessage);

            NameWAInterno.Add(WANumber);

            ButtonContatti.TouchUpInside += delegate { 
                if (keyboardOpen)
                {
                    WANumber.ResignFirstResponder();
                }
                Console.WriteLine("Send WA");
                if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
                    this.PresentModalViewController(_contactController2, true);
                else
                    this.PresentModalViewController(_contactController, true);
            };

            if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0)) {

                _contactController2 = new CNContactPickerViewController();

                /* Select property to pick
                picker.DisplayedPropertyKeys = new NSString[] { CNContactKey.EmailAddresses };
                picker.PredicateForEnablingContact = NSPredicate.FromFormat("emailAddresses.@count > 0");
                picker.PredicateForSelectionOfContact = NSPredicate.FromFormat("emailAddresses.@count == 1");

                */// Respond to selection
                _contactController2.Delegate = new ContactPickerDelegate();

            }
            else
            {
                _contactController = new ABPeoplePickerNavigationController();

                _contactController.Cancelled += delegate
                {
                    this.DismissModalViewController(true);
                };

                _contactController.SelectPerson2 += delegate (object sender, ABPeoplePickerSelectPerson2EventArgs e)
                {

                    //_contactName.Text = String.Format("{0} {1}", e.Person.FirstName, e.Person.LastName);
                    Console.WriteLine(e.Person.FirstName + "|" + e.Person.LastName + "|" + e.Person.GetPhones().GetValues()[0]);

                    string PhoneNumber = e.Person.GetPhones().GetValues()[0];
                    Console.WriteLine("1" + PhoneNumber);
                    if (PhoneNumber.Contains("+39") || PhoneNumber.Contains("+34") || PhoneNumber.Contains("+33") || PhoneNumber.Contains("+49") || PhoneNumber.Contains("+44") || PhoneNumber.Contains("+41") || PhoneNumber.Contains("+32") || PhoneNumber.Contains("+31"))
                        PhoneNumber = PhoneNumber.Substring(3);
                    if (PhoneNumber.Contains("+1"))
                        PhoneNumber = PhoneNumber.Substring(2);
                    if (PhoneNumber.Contains("+420") || PhoneNumber.Contains("+351") || PhoneNumber.Contains("+353") || PhoneNumber.Contains("+385") || PhoneNumber.Contains("+386"))
                        PhoneNumber = PhoneNumber.Substring(4);
                    Console.WriteLine(PhoneNumber);
                    PhoneNumber = PhoneNumber.Replace(" ", "");

                    WANumber.Text = PhoneNumber;
                    WANumber.TextColor = UIColor.Black;

                    PlaceholderWA = false;

                    this.DismissModalViewController(true);
                };
            }
        }

        public void SelectedChat(int index)
        {


            HomePage.Instance.name = ListPeople[index].Nome;
            HomePage.Instance.isFirstMessage = true;
            HomePage.Instance.isPreliminary = true;
            HomePage.Instance.FacebookId = ListPeople[index].ID;

            

            UIViewController lp = storyboard.InstantiateViewController("ChatPage");
            this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
            this.PresentViewController(lp, true, null);
        }

        public void RetriveFBPerson(string text)
        {
            string tokenFB = NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieFB");

            if (tokenFB != "")
            {
                var client = new RestClient("https://graph.facebook.com/");
                //DISTRIBUTION
                //var client = new RestClient("http://api.netwintec.com:3000"/");


                string name = text.Replace(" ", "%20");

                Console.WriteLine("https://graph.facebook.com/v2.5/search?q=" + name + "&type=user&field=id,name,picture&access_token=" + tokenFB + "&limit=50&offset=0");

                var requestN4U = new RestRequest("v2.5/search?q=" + name + "&type=user&field=id,name,picture&access_token=" + tokenFB + "&limit=50&offset=0", Method.GET);


                IRestResponse response = client.Execute(requestN4U);

                Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    JsonValue json = JsonValue.Parse(response.Content);
                    JsonValue value = json["data"];

                    foreach (JsonValue j in value)
                    {

                        string id = j["id"];
                        string nome = j["name"];
                        string picture = "https://graph.facebook.com/v2.5/" + id + "/picture?type=normal";

                        FbPerson c = new FbPerson(nome, id, picture);
                        ListPeople.Add(c);

                    }


                    //Console.WriteLine (response.Content);

                }
            }
            else
            {

                var client = new RestClient("http://api.netwintec.com:3000/");


                string name = text.Replace(" ", "%20");

                Console.WriteLine("https://graph.facebook.com/v2.5/search?q=" + name + "&type=user&field=id,name,picture&access_token=" + tokenFB + "&limit=50&offset=0");

                var requestN4U = new RestRequest("api/facebookSearch/" + name, Method.GET);
                requestN4U.AddHeader("content-type", "application/json");

                IRestResponse response = client.Execute(requestN4U);

                Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    JsonValue json = JsonValue.Parse(response.Content);
                    //JsonValue value = json["data"];

                    foreach (JsonValue j in json)
                    {

                        string id = j["id"];
                        string nome = j["name"];
                        string picture = "https://graph.facebook.com/v2.5/" + id + "/picture?type=normal";

                        FbPerson c = new FbPerson(nome, id, picture);
                        ListPeople.Add(c);

                    }


                    //Console.WriteLine (response.Content);

                }

            }


        }

        public void CompareNotification()
        {

            CABasicAnimation AlphaIn = new CABasicAnimation();
            AlphaIn.KeyPath = "opacity";
            AlphaIn.BeginTime = 0;
            AlphaIn.Duration = 1.5;
            AlphaIn.From = new NSNumber(0);
            AlphaIn.To = new NSNumber(1);

            CABasicAnimation AlphaOut = new CABasicAnimation();
            AlphaOut.KeyPath = "opacity";
            AlphaOut.BeginTime = 1.5;
            AlphaOut.Duration = 1.5;
            AlphaOut.From = new NSNumber(1);
            AlphaOut.To = new NSNumber(0);

            CAAnimationGroup animationGroup = CAAnimationGroup.CreateAnimation();
            animationGroup.Duration = 3;
            animationGroup.RepeatCount = 1;
            animationGroup.Animations = new CAAnimation[] { AlphaIn, AlphaOut };

            NotificationView.Layer.AddAnimation(animationGroup, null);
        }


        public void SelectContactRubrica(CNContact contact)  
        {
            //_contactName.Text = String.Format("{0} {1}", e.Person.FirstName, e.Person.LastName);
            Console.WriteLine(contact.GivenName + "|" + contact.PhoneticGivenName + "|" + contact.PhoneNumbers);

            string PhoneNumber = contact.PhoneNumbers[0].Value.StringValue;
            Console.WriteLine("1" + PhoneNumber);
            if (PhoneNumber.Contains("+39") || PhoneNumber.Contains("+34") || PhoneNumber.Contains("+33") || PhoneNumber.Contains("+49") || PhoneNumber.Contains("+44") || PhoneNumber.Contains("+41") || PhoneNumber.Contains("+32") || PhoneNumber.Contains("+31"))
                PhoneNumber = PhoneNumber.Substring(3);
            if (PhoneNumber.Contains("+1"))
                PhoneNumber = PhoneNumber.Substring(2);
            if (PhoneNumber.Contains("+420") || PhoneNumber.Contains("+351") || PhoneNumber.Contains("+353") || PhoneNumber.Contains("+385") || PhoneNumber.Contains("+386"))
                PhoneNumber = PhoneNumber.Substring(4);
            Console.WriteLine(PhoneNumber);
            PhoneNumber = PhoneNumber.Replace(" ", "");

            WANumber.Text = PhoneNumber;
            WANumber.TextColor = UIColor.Black;

            PlaceholderWA = false;

            this.DismissModalViewController(true);
        }
    }

    public class PickerModel : UIPickerViewModel
    {
        private readonly List<string> values;

        public event EventHandler PickerChanged;

        public int selectedCategoryID = 0;

        public PickerModel(List<string> collection)
        {
            this.values = collection;
        }

        public override nint GetComponentCount (UIPickerView picker)
        {
            return 1;
        }

        public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
        {
            return values.Count;
        }

        public override string GetTitle(UIPickerView pickerView, nint row, nint component)
        {
            return values[(int)row];
            
        }


        public override UIView GetView(UIPickerView pickerView, nint row, nint component, UIView view)
        {

            UILabel lbl = new UILabel(new CGRect(0, 0, 55f, 40f));
            lbl.TextColor = UIColor.Black;
            lbl.Font = UIFont.FromName("Roboto-Regular", 17f);
            lbl.TextAlignment = UITextAlignment.Center;
            lbl.Text = values[(int)row];
            return lbl;

        }

        public override nfloat GetRowHeight(UIPickerView pickerView, nint component)
        {
            return 50f;
        }

        public override void Selected(UIPickerView pickerView, nint row, nint component)
        {
            //base.Selected(pickerView, row, component);

            ContactPage.Instance.prefix = values[(int) row];
            Console.WriteLine(ContactPage.Instance.prefix);

        }

    
    }

    public class ContactPickerDelegate : CNContactPickerDelegate
    {

        #region Constructors
        public ContactPickerDelegate()
        {
        }

        public ContactPickerDelegate(IntPtr handle) : base(handle)
        {
        }
        #endregion

        #region Override Methods
        public override void ContactPickerDidCancel(CNContactPickerViewController picker)
        {
            Console.WriteLine("User canceled picker");

        }

        public override void DidSelectContact(CNContactPickerViewController picker, CNContact contact)
        {
            Console.WriteLine("Selected: {0}", contact.PhoneNumbers);

            ContactPage.Instance.SelectContactRubrica(contact);
        }

        public override void DidSelectContactProperty(CNContactPickerViewController picker, CNContactProperty contactProperty)
        {
            Console.WriteLine("Selected Property: {0}", contactProperty);
        }
        #endregion
    }
}
