// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Lovie
{
	[Register ("HomePage")]
	partial class HomePage
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView ChatTable { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView CompareView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel FBLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView FBView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel WALabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView WAView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ChatTable != null) {
				ChatTable.Dispose ();
				ChatTable = null;
			}
			if (CompareView != null) {
				CompareView.Dispose ();
				CompareView = null;
			}
			if (FBLabel != null) {
				FBLabel.Dispose ();
				FBLabel = null;
			}
			if (FBView != null) {
				FBView.Dispose ();
				FBView = null;
			}
			if (WALabel != null) {
				WALabel.Dispose ();
				WALabel = null;
			}
			if (WAView != null) {
				WAView.Dispose ();
				WAView = null;
			}
		}
	}
}
