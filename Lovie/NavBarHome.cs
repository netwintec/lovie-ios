using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Lovie
{
	partial class NavBarHome : UINavigationBar
	{
		public NavBarHome (IntPtr handle) : base (handle)
        { 
		}

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            //Console.WriteLine(Frame.Width);

            UIImageView centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 35, 4, 70, 36));
            centerlogo.Image = UIImage.FromFile("logo_lovie.png");

            AddSubview(centerlogo);

        }
    }
}
