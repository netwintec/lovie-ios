﻿using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Collections.Generic;
using RestSharp;
using System.Json;

namespace Lovie
{
	partial class HomePage : UIViewController
	{

        public UIStoryboard storyboard = UIStoryboard.FromName("Main", null);
        public List<Chat> list = new List<Chat>();
        public int chatSelected;
        bool CompareViewOpen = false;

        public UIView SopraView;
        public TableSourceHome HomeSource;

        public bool isPreliminary, isFirstMessage;
        public string Key, name, FacebookId,number;

        public HomePage (IntPtr handle) : base (handle)
		{
		}

        public static HomePage Instance{ get; private set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Instance = this;

            isPreliminary = false;
            isFirstMessage = false;
            Key = "";
            name = "";
            FacebookId = "";
            number = "";

            int h = 40;

            AppDelegate.Instance.isHome = true;
            AppDelegate.Instance.isChat = false;
            AppDelegate.Instance.isProfile = false;
            AppDelegate.Instance.isContact = false;
            AppDelegate.Instance.Key = "";

            RetriveData();

            HomeSource = new TableSourceHome(list);
            ChatTable.Source = HomeSource;
            ChatTable.SeparatorColor = UIColor.Clear;

            UITapGestureRecognizer FBViewTap = new UITapGestureRecognizer(() =>
            {
                Console.WriteLine("FACEBOOK");
            });
            FBView.UserInteractionEnabled = true;
            FBView.AddGestureRecognizer(FBViewTap);

            FBLabel.Font = UIFont.FromName("Roboto-Regular", 12f);

            UITapGestureRecognizer WAViewTap = new UITapGestureRecognizer(() =>
            {
                Console.WriteLine("WHATS APP");
            });
            WAView.UserInteractionEnabled = true;
            WAView.AddGestureRecognizer(WAViewTap);

            WALabel.Font = UIFont.FromName("Roboto-Regular", 12f);

            UIView AddChat = new UIView(new CGRect(0,View.Frame.Height-40,View.Frame.Width/2-2,40));
            AddChat.BackgroundColor=UIColor.FromRGB(63, 148, 202);
            UITapGestureRecognizer AddChatTap = new UITapGestureRecognizer(() =>
            {

                UIViewController lp = HomePage.Instance.storyboard.InstantiateViewController("ContactPage");
                HomePage.Instance.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                HomePage.Instance.PresentViewController(lp, true, null);
                /*
                if (CompareViewOpen)
                {
                    CompareViewOpen = false;
                    CompareView.Alpha = 0;
                }
                else
                {
                    CompareViewOpen = true;
                    CompareView.Alpha = 1;
                }*/

            });
            AddChat.UserInteractionEnabled = true;
            AddChat.AddGestureRecognizer(AddChatTap);

            UIImageView AddChatImage = new UIImageView(new CGRect(AddChat.Frame.Width/2-13,8,26,24));
            AddChatImage.Image = UIImage.FromFile("add_chat.png");


            UIView Profile = new UIView(new CGRect(View.Frame.Width / 2 + 2, View.Frame.Height - 40, View.Frame.Width / 2 - 2, 40));
            Profile.BackgroundColor = UIColor.FromRGB(87, 196, 241);
            UITapGestureRecognizer ProfileTap = new UITapGestureRecognizer(() =>
            {

                UIViewController lp = HomePage.Instance.storyboard.InstantiateViewController("ProfilePage");
                HomePage.Instance.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                HomePage.Instance.PresentViewController(lp, true, null);

            });
            Profile.UserInteractionEnabled = true;
            Profile.AddGestureRecognizer(ProfileTap);

            UIImageView ProfileImage = new UIImageView(new CGRect(Profile.Frame.Width / 2 - 12, 7, 24, 26));
            ProfileImage.Image = UIImage.FromFile("checkProfile.png");


            SopraView = new UIView(new CGRect(0, 64, View.Frame.Width , View.Frame.Height-64));
            SopraView.BackgroundColor = UIColor.FromRGBA(63, 148, 202,100);

            UIActivityIndicatorView loadspinner = new UIActivityIndicatorView(new CGRect(SopraView.Frame.Width/2 -20, SopraView.Frame.Height / 2 - 20, 40,40));
            loadspinner.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge;
            loadspinner.Color = UIColor.White;
            loadspinner.StartAnimating();

            AddChat.Add(AddChatImage);
            Profile.Add(ProfileImage);
            SopraView.Add(loadspinner);
     
            View.Add(AddChat);
            View.Add(Profile);
            View.Add(SopraView);

            /*bool push = NSUserDefaults.StandardUserDefaults.BoolForKey("isPushLovieNotification");
            Console.WriteLine("PUSH:" + push);
            if (push)
            {

                ChatPageAfterNot();

            }*/

        }

        public void ChatPageAfterNot()
        {
            Key = NSUserDefaults.StandardUserDefaults.StringForKey("KeyLovieNotification");
            name = NSUserDefaults.StandardUserDefaults.StringForKey("NameLovieNotification");
            isFirstMessage = false;
            isPreliminary = false;
            FacebookId = "";
            number = "";


            UIViewController lp = storyboard.InstantiateViewController("ChatPage");
            this.PresentModalViewController(lp, true);
        }


        public void SelectedChat(int index)
        {

            chatSelected = index;

            Key = list[index].Key;
            name = list[index].Nome;
            isFirstMessage = false;
            isPreliminary = list[index].isPreliminary;

            if (isPreliminary)
            {
                if (list[index].FacebookId != "")
                    FacebookId = list[index].FacebookId;
                if (list[index].Number != "")
                    number = list[index].Number;
            }

            UIViewController lp = storyboard.InstantiateViewController("ChatPage");
            this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
            this.PresentViewController(lp, true, null);
        }

        // ************** RETRIVE DATA FROM SERVER *******************
        public void RetriveData()
        {

            //var client = new RestClient("http://location.keepup.pro:3000/");
            //DISTRIBUTION
            var client = new RestClient("http://api.netwintec.com:3000/");

            string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U");

            if (token == "")
            {

                // ERRORE
                return;

            }

            var requestN4U = new RestRequest("api/conversations/get_conversations/" + token, Method.GET);
            requestN4U.AddHeader("content-type", "application/json");


            client.ExecuteAsync(requestN4U, (s, e) =>
            {

                IRestResponse response = s;

                Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    List<Chat> listApp = new List<Chat>();
                    JsonValue json = JsonValue.Parse(response.Content);

                    foreach (JsonValue j in json)
                    {

                        string key = j["_key"];
                        long date = j["lastUpdate"];

                        DateTime dateMessageApp = FromUnixTime(date);

                        NSTimeZone sourceTimeZone = new NSTimeZone("UTC");
                        NSTimeZone destinationTimeZone = NSTimeZone.LocalTimeZone;
                        NSDate sourceDate = DateTimeToNativeDate(dateMessageApp);

                        int sourceGMTOffset = (int)sourceTimeZone.SecondsFromGMT(sourceDate);
                        int destinationGMTOffset = (int)destinationTimeZone.SecondsFromGMT(sourceDate);
                        int interval = destinationGMTOffset - sourceGMTOffset;

                        Console.WriteLine("GMT:" + sourceGMTOffset + "|" + destinationGMTOffset + "|" + interval);

                        DateTime dateMessage = FromUnixTime(date + (interval * 1000));

                        //DateTime dateMessage = FromUnixTime(date);
                        bool SomethingNew = j["isSomethingNew"];
                        bool StartbyMe = j["isStartedByMe"];
                        bool isPreliminary = j["isPreliminary"];
                        string FacebookId = "";
                        string number = "";
                        if (isPreliminary)
                        {

                            string idApp = j["idUserTo"];
                            if (idApp.Length > 16)
                            {
                                FacebookId = idApp.Substring(16);
                                Console.WriteLine(FacebookId);
                            }
                            else
                            {
                                number = idApp;
                                Console.WriteLine(number);
                            }
                        }


                        string picture = "standard";
                        if (StartbyMe)
                        {
                            if (j["absolutePictureTo"] != null)
                            {
                                picture = j["absolutePictureTo"];
                            }
                        }
                        else
                        {
                            Console.WriteLine("picture:" + j["absolutePictureFrom"]);
                            if (j["absolutePictureFrom"] != null)
                            {
                                picture = j["absolutePictureFrom"];
                            }
                        }

                        var messages = j["messages"];
                        JsonValue jj = messages[0];
                        string message = jj["message"];
                        bool isMine = jj["isFromMe"];
                        string name = "";
                        if (isMine)
                        {
                            name = jj["to"];
                        }
                        else
                        {
                            name = jj["from"];
                        }

                        dateMessage.ToLocalTime();
                        if (DateTime.Now.IsDaylightSavingTime() == true)
                        {
                            Console.WriteLine("2");
                            dateMessage = dateMessage.AddHours(1);
                        }
                        Console.WriteLine("3a:" + dateMessage.ToString("yyyy-MM-dd") + " " + dateMessage.ToString("HH:mm:dd"));

                        Chat c = new Chat(key, name, message, dateMessage, SomethingNew, picture, isPreliminary, FacebookId, number);
                        listApp.Add(c);

                    }

                    InvokeOnMainThread(() => {
                        for (int i = listApp.Count - 1; i >= 0; i--)
                        {
                            Console.WriteLine("LISTAPP:" + i + ":" + listApp[i].Number + ":" + listApp[i].Nome);
                            list.Add(listApp[i]);
                            Console.WriteLine("LIST:" + HomeSource.element + ":" + list[HomeSource.element].Number + ":" + list[HomeSource.element].Nome);
                            //HomeSource.listChat.Add(listApp[i]);
                            Console.WriteLine("LISTCHAT:" + HomeSource.element + ":" + HomeSource.listChat[HomeSource.element].Number + ":" + HomeSource.listChat[HomeSource.element].Nome);
                            HomeSource.element++;
                            //Enumerable.Reverse (list);
                        }

                        ChatTable.ReloadData();
                        SopraView.RemoveFromSuperview();
                        //Console.WriteLine (response.Content);
                        bool push = NSUserDefaults.StandardUserDefaults.BoolForKey("isPushLovieNotification");
                        Console.WriteLine("PUSH:" + push);
                        if (push)
                        {

                            ChatPageAfterNot();

                        }
                    });
                }
            });


        }

        // ************** RETRIVE DATA FROM SERVER AFTER NOTIFICATION *******************

        public void ChangeDataNotification()
        {

            //var client = new RestClient("http://location.keepup.pro:3000/");
            //DISTRIBUTION
            var client = new RestClient("http://api.netwintec.com:3000/");

            string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U");

            if (token == "")
            {

                // ERRORE
                return;

            }

            var requestN4U = new RestRequest("api/conversations/get_conversations/" + token, Method.GET);
            requestN4U.AddHeader("content-type", "application/json");


            client.ExecuteAsync(requestN4U, (s, e) =>
             {

                 IRestResponse response = s;

                 Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);
                 if (response.StatusCode == System.Net.HttpStatusCode.OK)
                 {
                     List<Chat> listApp = new List<Chat>();
                     JsonValue json = JsonValue.Parse(response.Content);

                     foreach (JsonValue j in json)
                     {

                         string key = j["_key"];
                         long date = j["lastUpdate"];

                         DateTime dateMessageApp = FromUnixTime(date);

                         NSTimeZone sourceTimeZone = new NSTimeZone("UTC");
                         NSTimeZone destinationTimeZone = NSTimeZone.LocalTimeZone;
                         NSDate sourceDate = DateTimeToNativeDate(dateMessageApp);

                         int sourceGMTOffset = (int)sourceTimeZone.SecondsFromGMT(sourceDate);
                         int destinationGMTOffset = (int)destinationTimeZone.SecondsFromGMT(sourceDate);
                         int interval = destinationGMTOffset - sourceGMTOffset;

                         Console.WriteLine("GMT:" + sourceGMTOffset + "|" + destinationGMTOffset + "|" + interval);

                         DateTime dateMessage = FromUnixTime(date + (interval * 1000));

                         bool SomethingNew = j["isSomethingNew"];
                         bool StartbyMe = j["isStartedByMe"];
                         bool isPreliminary = j["isPreliminary"];
                         string FacebookId = "";
                         string number = "";
                         if (isPreliminary)
                         {
                             
                             string idApp = j["idUserTo"];
                             if (idApp.Length > 16)
                             {
                                 FacebookId = idApp.Substring(16);
                                 Console.WriteLine(FacebookId);
                             }
                             else
                             {
                                 number = idApp;
                                 Console.WriteLine(number);
                             }
                         }


                         string picture = "standard";
                         if (StartbyMe)
                         {
                             if (j["absolutePictureTo"] != null)
                             {
                                 picture = j["absolutePictureTo"];
                             }
                         }
                         else
                         {
                             Console.WriteLine("picture:" + j["absolutePictureFrom"]);
                             if (j["absolutePictureFrom"] != null)
                             {
                                 picture = j["absolutePictureFrom"];
                             }
                         }

                         var messages = j["messages"];
                         JsonValue jj = messages[0];
                         string message = jj["message"];
                         bool isMine = jj["isFromMe"];
                         string name = "";
                         if (isMine)
                         {
                             name = jj["to"];
                         }
                         else
                         {
                             name = jj["from"];
                         }

                         dateMessage.ToLocalTime();
                         if (DateTime.Now.IsDaylightSavingTime() == true)
                         {
                             Console.WriteLine("2");
                             dateMessage = dateMessage.AddHours(1);
                         }
                         Console.WriteLine("3a:" + dateMessage.ToString("yyyy-MM-dd") + " " + dateMessage.ToString("HH:mm:dd"));

                         Chat c = new Chat(key, name, message, dateMessage, SomethingNew, picture, isPreliminary, FacebookId,number);
                         listApp.Add(c);

                     }

                     InvokeOnMainThread(() => {

                         list.Clear();
                         HomeSource.element = 0;
                         for (int i = listApp.Count - 1; i >= 0; i--)
                         {
                             Console.WriteLine("LISTAPP:"+i+":"+listApp[i].Number+":"+ listApp[i].Nome);
                             list.Add(listApp[i]);
                             Console.WriteLine("LIST:" + HomeSource.element + ":" + list[HomeSource.element].Number + ":" + list[HomeSource.element].Nome);
                             //HomeSource.listChat.Add(listApp[i]);
                             Console.WriteLine("LISTCHAT:"+HomeSource.element + ":" + HomeSource.listChat[HomeSource.element].Number + ":" + HomeSource.listChat[HomeSource.element].Nome);
                             HomeSource.element++;
                             //Enumerable.Reverse (list);
                         }

                         ChatTable.ReloadData();
                         SopraView.RemoveFromSuperview();
                         //Console.WriteLine (response.Content);
                     });
                 }
             });


        }



        public static NSDate DateTimeToNativeDate(DateTime date)
        {
            DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(
                new DateTime(2001, 1, 1, 0, 0, 0));
            return NSDate.FromTimeIntervalSinceReferenceDate(
                (date - reference).TotalSeconds);
        }

        public DateTime FromUnixTime(long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddMilliseconds(unixTime);
        }

    }
}
