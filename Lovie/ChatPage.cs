﻿using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using UIKit;
using System.Json;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Threading.Tasks;
using ToastIOS;
using System.Threading;
using CoreAnimation;

namespace Lovie
{
	partial class ChatPage : UIViewController
	{

        public const string ERR_MAX_PRELIMINAR_MESSAGE = "Max preliminary message for this person reached";
        public const string ERR_NOT_ENOUGHT_CREDIT = "Credit is not enough";

        public TableSourceChat Chat;
        bool keyboardVisible;
        float KeyboardHeight;
        UITableView chatTable;
        UIView EnterView;
        UITextView message;
        UIImageView SendButton;
        UIView TrasparentView;

        int messageRow;
        bool PlaceholderText = true;
        bool ScrollToEnd = true;
        bool HintVisible;
        bool isRevealed = true;

        public UIStoryboard storyboard = UIStoryboard.FromName("Main", null);
        List<Message> list = new List<Message>();
        string key,name,FacebookId,number;
        bool isPreliminary, isFirstMessage;

        public string SegnalaKey, SegnalaPersonaName ,SegnalaPersonaId;

        public UIView SopraView;
        public ChatPage (IntPtr handle) : base (handle)
		{
		}

        public static ChatPage Instance { get; private set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            int h = 40;

            ChatPage.Instance = this;

            AppDelegate.Instance.isHome = false;
            AppDelegate.Instance.isChat = true;
            AppDelegate.Instance.isProfile = false;
            AppDelegate.Instance.isContact = false;
            AppDelegate.Instance.Key = HomePage.Instance.Key;

            NSUserDefaults.StandardUserDefaults.SetBool(false, "isPushLovieNotification");

            key = HomePage.Instance.Key;
            name = HomePage.Instance.name;
            FacebookId = HomePage.Instance.FacebookId;
            isPreliminary = HomePage.Instance.isPreliminary;
            isFirstMessage = HomePage.Instance.isFirstMessage;
            number = HomePage.Instance.number;

            SopraView = new UIView(new CGRect(0, 64, View.Frame.Width, View.Frame.Height - 64));
            SopraView.BackgroundColor = UIColor.FromRGBA(63, 148, 202, 100);

            UIActivityIndicatorView loadspinner = new UIActivityIndicatorView(new CGRect(SopraView.Frame.Width / 2 - 20, SopraView.Frame.Height / 2 - 20, 40, 40));
            loadspinner.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge;
            loadspinner.Color = UIColor.White;
            loadspinner.StartAnimating();

            SopraView.Add(loadspinner);

            Chat = new TableSourceChat(list, (float)View.Frame.Width, this);

            //Tabella
            chatTable = new UITableView(new CGRect(0, 69, View.Frame.Width, View.Frame.Height - 129));
            chatTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            chatTable.Source = Chat;
            chatTable.SeparatorColor = UIColor.Clear;
            if (list.Count > 0)
                chatTable.ScrollToRow(NSIndexPath.FromRowSection(list.Count - 1, 0), UITableViewScrollPosition.Bottom, true);


            View.Add(chatTable);

            if (!isFirstMessage)
                RetriveMessages();

            //View Nome Cognome
            TrasparentView = new UIView(new CGRect(0, 64, View.Frame.Width, 40));
            TrasparentView.BackgroundColor = UIColor.FromRGBA(76, 156, 206, 220);

            UILabel Name = new UILabel(new CGRect(0, (TrasparentView.Frame.Height / 2) - 10, TrasparentView.Frame.Width, 20));
            Name.TextColor = UIColor.White;
            Name.BackgroundColor = UIColor.Clear;
            Name.TextAlignment = UITextAlignment.Center;
            Name.Text = name;
            Name.Font = UIFont.FromName("Roboto-Regular", 15);

            UIView WhiteView = new UIView(new CGRect(0, 39, View.Frame.Width, 1));
            WhiteView.BackgroundColor = UIColor.White;

            TrasparentView.Add(Name);
            TrasparentView.Add(WhiteView);

            View.Add(TrasparentView);

            //View Inserimento
            EnterView = new UIView(new CGRect(0, 69 + chatTable.Frame.Height, View.Frame.Width, 60));

            UIView separator = new UIView(new CGRect(0, 0, View.Frame.Width, 2));
            separator.BackgroundColor = UIColor.FromRGB(76, 156, 206);

            SendButton = new UIImageView(new CGRect(View.Frame.Width - 45, 15, 30, 30));
            SendButton.Image = UIImage.FromFile("send_message.png");
            UITapGestureRecognizer SendMessage = new UITapGestureRecognizer(() =>
            {
                if (!string.IsNullOrWhiteSpace(message.Text) && !string.IsNullOrEmpty(message.Text) && !PlaceholderText)
                {
                    if (!isPreliminary)
                    {
                        Console.WriteLine("MESSAGE:" + message.Text);
                        // ******** SEND MESSAGE **************
                        //var client = new RestClient("http://location.keepup.pro:3000/");
                        //DISTRIBUTION
                        var client = new RestClient("http://api.netwintec.com:3000/");

                        string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U");

                        if (token != "")
                        {

                            var requestN4U = new RestRequest("api/conversations/message", Method.POST);
                            requestN4U.AddHeader("content-type", "application/json");
                            requestN4U.AddHeader("lovie-token", token);
                            requestN4U.AddHeader("lovie-conversation-token", key);

                            JObject oJsonObject = new JObject();

                            oJsonObject.Add("message", message.Text);

                            requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                            IRestResponse response = client.Execute(requestN4U);

                            Console.WriteLine(response.Content + "|" + response.StatusCode);

                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                CGRect frame = chatTable.Frame;

                                if (messageRow == 2)
                                    frame.Height += 20;
                                if (messageRow == 3)
                                    frame.Height += 40;
                                if (messageRow == 4)
                                    frame.Height += 60;
                                chatTable.Frame = frame;

                                //chatTable.Frame = new CGRect(0, 69, View.Frame.Width, View.Frame.Height - 144);
                                EnterView.Frame = new CGRect(0, 69 + chatTable.Frame.Height, View.Frame.Width, 60);
                                message.Frame = new CGRect(15, 15, View.Frame.Width - 80, 30);
                                SendButton.Frame = new CGRect(View.Frame.Width - 45, 15, 30, 30);
                                View.SetNeedsLayout();

                                Chat.listMessage.Add(new Message(message.Text, true));
                                Chat.element++;
                                chatTable.ReloadData();
                                if (Chat.element > 0)
                                    chatTable.ScrollToRow(NSIndexPath.FromRowSection(Chat.element - 1, 0), UITableViewScrollPosition.Top, true);
                                message.Text = "Scrivi un messaggio";
                                PlaceholderText = true;
                                message.TextColor = UIColor.FromRGB(200, 200, 200);

                                if (keyboardVisible)
                                {
                                    ScrollToEnd = true;
                                    message.ResignFirstResponder();
                                }
                                messageRow = 1;
                            }
                            if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                            {
                                CGRect frame = chatTable.Frame;

                                if (messageRow == 2)
                                    frame.Height += 20;
                                if (messageRow == 3)
                                    frame.Height += 40;
                                if (messageRow == 4)
                                    frame.Height += 60;
                                chatTable.Frame = frame;

                                if (keyboardVisible)
                                {
                                    message.ResignFirstResponder();
                                }

                                //chatTable.Frame = new CGRect(0, 69, View.Frame.Width, View.Frame.Height - 144);
                                EnterView.Frame = new CGRect(0, 69 + chatTable.Frame.Height, View.Frame.Width, 60);
                                message.Frame = new CGRect(15, 15, View.Frame.Width - 80, 30);
                                SendButton.Frame = new CGRect(View.Frame.Width - 45, 15, 30, 30);
                                View.SetNeedsLayout();
                                if (response.Content.CompareTo(ERR_MAX_PRELIMINAR_MESSAGE) == 0)
                                {
                                    var adErr = new UIAlertView("Messaggio Preliminare", "Hai raggiunto il numero massimo (3) di messaggi inviabili ad un utente non registrato aspetta che ti risponda per poter continuare", null, "Ok", null);
                                    adErr.Show();
                                }
                                if (response.Content.CompareTo(ERR_NOT_ENOUGHT_CREDIT) == 0)
                                {
                                    var adErr = new UIAlertView("Credito Indufficente", "Credito insufficente, ricarica per poter continuare a chattare", null, "Ok", null);
                                    adErr.Show();
                                }

                            }
                        }
                    }
                    else
                    {

                        //PRELIMINARY NUMBER
                        if (number != "")
                        {
                            Console.WriteLine("PRELIMINARY_WA_MESSAGE:" + message.Text + "|" + number);
                            // ******** SEND PRELIMINARY MESSAGE **************
                            //var client = new RestClient("http://location.keepup.pro:3000/");
                            //DISTRIBUTION
                            var client = new RestClient("http://api.netwintec.com:3000/");

                            string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U");

                            if (token != "")
                            {

                                var requestN4U = new RestRequest("api/conversations/preliminary_message", Method.POST);
                                requestN4U.AddHeader("content-type", "application/json");
                                requestN4U.AddHeader("lovie-token", token);

                                JObject oJsonObject = new JObject();

                                oJsonObject.Add("message", message.Text);
                                oJsonObject.Add("phone", number);

                                requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                                IRestResponse response = client.Execute(requestN4U);

                                Console.WriteLine(response.Content + "|" + response.StatusCode);

                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    CGRect frame = chatTable.Frame;

                                    if (messageRow == 2)
                                        frame.Height += 20;
                                    if (messageRow == 3)
                                        frame.Height += 40;
                                    if (messageRow == 4)
                                        frame.Height += 60;
                                    chatTable.Frame = frame;

                                    //chatTable.Frame = new CGRect(0, 69, View.Frame.Width, View.Frame.Height - 144);
                                    EnterView.Frame = new CGRect(0, 69 + chatTable.Frame.Height, View.Frame.Width, 60);
                                    message.Frame = new CGRect(15, 15, View.Frame.Width - 80, 30);
                                    SendButton.Frame = new CGRect(View.Frame.Width - 45, 15, 30, 30);
                                    View.SetNeedsLayout();

                                    Chat.listMessage.Add(new Message(message.Text, true));
                                    Chat.element++;
                                    chatTable.ReloadData();
                                    if (Chat.element > 0)
                                        chatTable.ScrollToRow(NSIndexPath.FromRowSection(Chat.element - 1, 0), UITableViewScrollPosition.Top, true);
                                    message.Text = "Scrivi un messaggio";
                                    PlaceholderText = true;
                                    message.TextColor = UIColor.FromRGB(200, 200, 200);

                                    if (keyboardVisible)
                                    {
                                        ScrollToEnd = true;
                                        message.ResignFirstResponder();
                                    }
                                    messageRow = 1;
                                }
                                if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                                {

                                    CGRect frame = chatTable.Frame;

                                    if (messageRow == 2)
                                        frame.Height += 20;
                                    if (messageRow == 3)
                                        frame.Height += 40;
                                    if (messageRow == 4)
                                        frame.Height += 60;
                                    chatTable.Frame = frame;

                                    if (keyboardVisible)
                                    {
                                        message.ResignFirstResponder();
                                    }

                                    if (response.Content.CompareTo(ERR_MAX_PRELIMINAR_MESSAGE) == 0)
                                    {
                                        var adErr = new UIAlertView("Messaggio Preliminare", "Hai raggiunto il numero massimo (3) di messaggi inviabili ad un utente non registrato aspetta che ti risponda per poter continuare", null, "Ok", null);
                                        adErr.Show();
                                    }
                                    if (response.Content.CompareTo(ERR_NOT_ENOUGHT_CREDIT) == 0)
                                    {
                                        var adErr = new UIAlertView("Credito Indufficente", "Credito insufficente, ricarica per poter continuare a chattare", null, "Ok", null);
                                        adErr.Show();
                                    }

                                }
                            }
                        }

                        //PRELIMINARY FB
                        if (FacebookId != "")
                        {
                            Console.WriteLine("PRELIMINARY_FB_MESSAGE:" + message.Text + "|" + FacebookId);
                            // ******** SEND PRELIMINARY MESSAGE **************
                            //var client = new RestClient("http://location.keepup.pro:3000/");
                            //DISTRIBUTION
                            var client = new RestClient("http://api.netwintec.com:3000/");

                            string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U");

                            if (token != "")
                            {

                                var requestN4U = new RestRequest("api/conversations/preliminary_message", Method.POST);
                                requestN4U.AddHeader("content-type", "application/json");
                                requestN4U.AddHeader("lovie-token", token);

                                JObject oJsonObject = new JObject();

                                oJsonObject.Add("message", message.Text);
                                oJsonObject.Add("socialId", FacebookId);
                                oJsonObject.Add("socialFullName", name);
                                oJsonObject.Add("socialName", "facebook");

                                requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);

                                IRestResponse response = client.Execute(requestN4U);

                                Console.WriteLine(response.Content + "|" + response.StatusCode);

                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    CGRect frame = chatTable.Frame;

                                    if (messageRow == 2)
                                        frame.Height += 20;
                                    if (messageRow == 3)
                                        frame.Height += 40;
                                    if (messageRow == 4)
                                        frame.Height += 60;
                                    chatTable.Frame = frame;

                                    //chatTable.Frame = new CGRect(0, 69, View.Frame.Width, View.Frame.Height - 144);
                                    EnterView.Frame = new CGRect(0, 69 + chatTable.Frame.Height, View.Frame.Width, 60);
                                    message.Frame = new CGRect(15, 15, View.Frame.Width - 80, 30);
                                    SendButton.Frame = new CGRect(View.Frame.Width - 45, 15, 30, 30);
                                    View.SetNeedsLayout();

                                    Chat.listMessage.Add(new Message(message.Text, true));
                                    Chat.element++;
                                    chatTable.ReloadData();
                                    if (Chat.element > 0)
                                        chatTable.ScrollToRow(NSIndexPath.FromRowSection(Chat.element - 1, 0), UITableViewScrollPosition.Top, true);
                                    message.Text = "Scrivi un messaggio";
                                    PlaceholderText = true;
                                    message.TextColor = UIColor.FromRGB(200, 200, 200);

                                    if (keyboardVisible)
                                    {
                                        ScrollToEnd = true;
                                        message.ResignFirstResponder();
                                    }
                                    messageRow = 1;
                                }
                                if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                                {

                                    CGRect frame = chatTable.Frame;

                                    if (messageRow == 2)
                                        frame.Height += 20;
                                    if (messageRow == 3)
                                        frame.Height += 40;
                                    if (messageRow == 4)
                                        frame.Height += 60;
                                    chatTable.Frame = frame;

                                    if (keyboardVisible)
                                    {
                                        message.ResignFirstResponder();
                                    }

                                    if (response.Content.CompareTo(ERR_MAX_PRELIMINAR_MESSAGE) == 0)
                                    {
                                        var adErr = new UIAlertView("Messaggio Preliminare", "Hai raggiunto il numero massimo (3) di messaggi inviabili ad un utente non registrato aspetta che ti risponda per poter continuare", null, "Ok", null);
                                        adErr.Show();
                                    }
                                    if (response.Content.CompareTo(ERR_NOT_ENOUGHT_CREDIT) == 0)
                                    {
                                        var adErr = new UIAlertView("Credito Indufficente", "Credito insufficente, ricarica per poter continuare a chattare", null, "Ok", null);
                                        adErr.Show();
                                    }

                                }
                            }
                        }
                    }
                }
            });
            SendButton.UserInteractionEnabled = true;
            SendButton.AddGestureRecognizer(SendMessage);

            message = new UITextView(new CGRect(15, 15, View.Frame.Width - 80, 30));
            message.KeyboardType = UIKeyboardType.Default;
            message.Text = "Scrivi un messaggio";
            
            messageRow = 1;

            //message.Placeholder = "Scrivi un messaggio";

            //NSAttributedString a = new NSAttributedString("Scrivi un messaggio",UIFont.FromName("Roboto-Regular",17), null, null, null, null,NSLigatureType.Default,0,NSUnderlineStyle.Single, null, 0,NSUnderlineStyle.None);   
            //message.AttributedPlaceholder = a;

            message.TextColor = UIColor.FromRGB(200, 200, 200);
            message.Font = UIFont.FromName("Roboto-Regular", 17);
            //message.BorderStyle = UITextBorderStyle.None;

            message.ShouldBeginEditing += (UITextField) => {
                if (message.Text == "Scrivi un messaggio")
                {
                    message.Text = "";
                    message.TextColor = UIColor.Black;
                }
                PlaceholderText = false;
                return true;
            };

            message.Changed += (s, e) => {

                var size = UIStringDrawing.StringSize(message.Text + "d", UIFont.FromName("Roboto-Regular", 17), new CGSize(View.Frame.Width - 80, 2000));
                Console.WriteLine(size.Height + "  " + size.Width);

                if (size.Height <= 20 && messageRow != 1)
                {
                    Console.WriteLine("PROVA 2:" + messageRow);

                    CGRect frame = chatTable.Frame;
                    if (messageRow == 2)
                        frame.Height += 20;
                    if (messageRow == 3)
                        frame.Height += 40;
                    if (messageRow == 4)
                        frame.Height += 60;
                    chatTable.Frame = frame;

                    //chatTable.Frame = new CGRect(0, 69, View.Frame.Width, View.Frame.Height - 144);
                    EnterView.Frame = new CGRect(0, 69 + chatTable.Frame.Height, View.Frame.Width, 60);
                    message.Frame = new CGRect(15, 15, View.Frame.Width - 80, 30);
                    SendButton.Frame = new CGRect(View.Frame.Width - 45, 15, 30, 30);
                    View.SetNeedsLayout();

                    messageRow = 1;
                }

                if (size.Height > 20 && size.Height <= 40 && messageRow != 2)
                {
                    Console.WriteLine("PROVA 2:" + messageRow);

                    CGRect frame = chatTable.Frame;
                    if (messageRow == 1)
                        frame.Height -= 20;
                    if (messageRow == 3)
                        frame.Height += 20;
                    chatTable.Frame = frame;

                    //chatTable.Frame = new CGRect(0, 69, View.Frame.Width, View.Frame.Height - 144);
                    EnterView.Frame = new CGRect(0, 69 + chatTable.Frame.Height, View.Frame.Width, 80);
                    message.Frame = new CGRect(15, 15, View.Frame.Width - 80, 50);
                    SendButton.Frame = new CGRect(View.Frame.Width - 45, 25, 30, 30);
                    View.SetNeedsLayout();

                    messageRow = 2;
                }

                if (size.Height > 40 && size.Height <= 60 && messageRow != 3)
                {
                    Console.WriteLine("PROVA 3:" + messageRow);

                    CGRect frame = chatTable.Frame;
                    if (messageRow == 2)
                        frame.Height -= 20;
                    if (messageRow == 4)
                        frame.Height += 20;
                    chatTable.Frame = frame;

                    //chatTable.Frame = new CGRect(0, 69, View.Frame.Width, View.Frame.Height - 144);
                    EnterView.Frame = new CGRect(0, 69 + chatTable.Frame.Height, View.Frame.Width, 100);
                    message.Frame = new CGRect(15, 15, View.Frame.Width - 80, 70);
                    SendButton.Frame = new CGRect(View.Frame.Width - 45, 35, 30, 30);
                    View.SetNeedsLayout();

                    messageRow = 3;
                }

                if (size.Height > 60 && size.Height <= 80 && messageRow != 4)
                {
                    Console.WriteLine("PROVA 4:" + messageRow);

                    CGRect frame = chatTable.Frame;
                    if (messageRow == 3)
                        frame.Height -= 20;
                    chatTable.Frame = frame;

                    //chatTable.Frame = new CGRect(0, 69, View.Frame.Width, View.Frame.Height - 144);
                    EnterView.Frame = new CGRect(0, 69 + chatTable.Frame.Height, View.Frame.Width, 120);
                    message.Frame = new CGRect(15, 15, View.Frame.Width - 80, 90);
                    SendButton.Frame = new CGRect(View.Frame.Width - 45, 45, 30, 30);
                    View.SetNeedsLayout();

                    messageRow = 4;
                }


            };

            message.ShouldEndEditing += (UITextField) => {

                Console.WriteLine("Fine:" + keyboardVisible + "|" + KeyboardHeight);
                if (string.IsNullOrEmpty(message.Text) || string.IsNullOrWhiteSpace(message.Text))
                {
                    message.Text = "Scrivi un messaggio";
                    PlaceholderText = true;
                    message.TextColor = UIColor.FromRGB(200, 200, 200);
                }
                ChangeView();
                return true;
            };

            EnterView.Add(separator);
            EnterView.Add(SendButton);
            EnterView.Add(message);
            View.Add(EnterView);



            NSObject _notification = UIKeyboard.Notifications.ObserveWillShow((s, e) =>
            {
                var r = UIKeyboard.FrameBeginFromNotification(e.Notification);
                KeyboardHeight = (float)r.Height;

                if (KeyboardHeight < 240)
                {
                    HintVisible = false; }

                if (KeyboardHeight >= 240)
                {
                    HintVisible = true;
                }

                Console.WriteLine(KeyboardHeight);
                if (!keyboardVisible)
                    ChangeView();
                else
                    SeeHint();

            });

            revealButton.Clicked += async delegate
            {

                if (!isRevealed) { 
                    int button = await ShowAlert("Rivelati", "Vuoi veramente rivelarti?", "Ok", "Annulla");
                    Console.WriteLine("Click" + button);


                    if (button == 0)
                    {

                        //var client = new RestClient("http://location.keepup.pro:3000/");
                        //DISTRIBUTION
                        var client = new RestClient("http://api.netwintec.com:3000/");

                        string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U");

                        if (token == "")
                        {

                            // ERRORE
                            return;

                        }

                        var requestN4U = new RestRequest("api/conversations/reveal", Method.POST);
                        requestN4U.AddHeader("content-type", "application/json");
                        requestN4U.AddHeader("lovie-token", token);
                        requestN4U.AddHeader("lovie-conversation-token", key);

                        IRestResponse response = client.Execute(requestN4U);

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            navBar.RemoveReveal();
                            isRevealed = true;
                            Toast.MakeText("Ti sei Rivelato con successo", Toast.LENGTH_LONG).Show();
                        }
                        else {
                            isRevealed = false;
                            Toast.MakeText("Errore nella rilevazione provare più tardi", Toast.LENGTH_LONG).Show();
                        }
                    }
                }
            };


            SegnalaIcon.Clicked += async delegate
            {
                if (isPreliminary)
                {
                    var adErr = new UIAlertView("Errore", "Impossibile segnalare un utente che non è registrato all'app", null, "Continua", null);
                    adErr.Show();

                }
                else
                {
                    if (keyboardVisible)
                    {
                        message.ResignFirstResponder();
                    }

                    var alert = new UIAlertView
                    {
                        AlertViewStyle = UIAlertViewStyle.PlainTextInput,
                        Title = "Segnala",
                        Message = "Inviaci una segnalazione per questa conversazione specificando , se vuoi, il motivo della segnalazione."
                    };

                    alert.GetTextField(0).Placeholder = "Motivo segnalazione";

                    alert.GetTextField(0).ShouldEndEditing += (UITextField) =>
                    {

                        Console.WriteLine("Fine:" + keyboardVisible + "|" + KeyboardHeight);
                        ChangeView();
                        return true;
                    };

                    int button = await ShowAlertText(alert, "Ok", "Annulla");
                    Console.WriteLine("Click" + button);

                    if (button == 0)
                    {
                        //var client = new RestClient("http://location.keepup.pro:3000/");
                        //DISTRIBUTION
                        var client = new RestClient("http://api.netwintec.com:3000/");

                        string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U");

                        if (token == "")
                        {

                            // ERRORE
                            return;

                        }

                        var requestN4U = new RestRequest("api/conversations/report", Method.POST);
                        requestN4U.AddHeader("content-type", "application/json");
                        requestN4U.AddHeader("lovie-token", token);
                        requestN4U.AddHeader("lovie-conversation-token", key);

                        JObject oJsonObject = new JObject();

                        oJsonObject.Add("description", alert.GetTextField(0).Text);

                        requestN4U.AddParameter("application/json; charset=utf-8", oJsonObject, ParameterType.RequestBody);


                        client.ExecuteAsync(requestN4U, (s, e) =>
                        {

                            InvokeOnMainThread(() =>
                            {

                                IRestResponse response = s;
                                Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);
                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    InvokeOnMainThread(() =>
                                    {
                                        Toast.MakeText("Segnalazione avvenuta con successo", Toast.LENGTH_LONG).Show();
                                    });
                                }
                                else
                                {
                                    InvokeOnMainThread(() =>
                                    {
                                        Toast.MakeText("Segnalazione avvenuta con successo", Toast.LENGTH_LONG).Show();
                                    });
                                }
                            });
                        });

                        Console.WriteLine(alert.GetTextField(0).Text + "|" + SegnalaKey + "|" + SegnalaPersonaId + "|" + SegnalaPersonaName);

                    }
                }

            };

            if (!isFirstMessage)
                View.Add(SopraView);
        }


        public static Task<int> ShowAlertText(UIAlertView alert, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();

            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }
        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public void SeeHint()
        {
            if (keyboardVisible)
            {
                if (KeyboardHeight < 240)
                {
                    HintVisible = true;
                    CGRect frame = chatTable.Frame;
                    frame.Height -= 29;
                    //chatTable.ScrollToRow(NSIndexPath.FromRowSection(Chat.element - 1, 0), UITableViewScrollPosition.Top, true);
                    chatTable.Frame = frame;

                    frame = EnterView.Frame;
                    frame.Y -= 29;
                    EnterView.Frame = frame;
                    
                }

                if (KeyboardHeight > 240)
                {
                    HintVisible = false;
                    CGRect frame = chatTable.Frame;
                    frame.Height += 29;
                    //chatTable.ScrollToRow(NSIndexPath.FromRowSection(Chat.element - 1, 0), UITableViewScrollPosition.Top, true);
                    chatTable.Frame = frame;

                    frame = EnterView.Frame;
                    frame.Y += 29;
                    EnterView.Frame = frame;

                }

            }
        }

        public void ChangeView()
        {
            if (!keyboardVisible)
            {
                CGRect frame = chatTable.Frame;
                frame.Height -= KeyboardHeight;
                chatTable.Frame = frame;
                if(Chat.element > 0)
                    chatTable.ScrollToRow(NSIndexPath.FromRowSection(Chat.element - 1, 0), UITableViewScrollPosition.Bottom, true);

                frame = TrasparentView.Frame;
                frame.Y = 69;
                TrasparentView.Frame = frame;

                frame = EnterView.Frame;
                frame.Y -= KeyboardHeight;
                EnterView.Frame = frame;
                keyboardVisible = true;
            }
            else
            {
                Console.WriteLine("STE:"+ScrollToEnd);
                if (HintVisible)
                {
                    KeyboardHeight = 253;
                }
                else
                {
                    KeyboardHeight = 224;
                }
                CGRect frame = chatTable.Frame;
                frame.Height += KeyboardHeight;
                chatTable.Frame = frame;
                if (ScrollToEnd)
                {
                    if (Chat.element > 0)
                        chatTable.ScrollToRow(NSIndexPath.FromRowSection(Chat.element - 1, 0), UITableViewScrollPosition.Bottom, true);
                }
                frame = TrasparentView.Frame;
                frame.Height = 40;
                TrasparentView.Frame = frame;

                frame = EnterView.Frame;
                frame.Y += KeyboardHeight;
                EnterView.Frame = frame;
                keyboardVisible = false;
            }

        }

        public void ToggleKeyboard()
        {
            Console.WriteLine("TK:" + string.IsNullOrEmpty(message.Text) + "|" + string.IsNullOrWhiteSpace(message.Text));

            if (string.IsNullOrEmpty(message.Text) || string.IsNullOrWhiteSpace(message.Text)) { 
                CGRect frame = chatTable.Frame;

                if (messageRow == 2)
                    frame.Height += 20;
                if (messageRow == 3)
                    frame.Height += 40;
                if (messageRow == 4)
                    frame.Height += 60;
                chatTable.Frame = frame;

                //chatTable.Frame = new CGRect(0, 69, View.Frame.Width, View.Frame.Height - 144);
                EnterView.Frame = new CGRect(0, 69 + chatTable.Frame.Height, View.Frame.Width, 60);
                message.Frame = new CGRect(15, 15, View.Frame.Width - 80, 30);
                SendButton.Frame = new CGRect(View.Frame.Width - 45, 15, 30, 30);
                View.SetNeedsLayout();

                if (keyboardVisible)
                {
                    ScrollToEnd = false;
                    message.ResignFirstResponder();
                }

                //message.Text = "";

                messageRow = 1;
            }

            Console.WriteLine("TK:" + !string.IsNullOrEmpty(message.Text) + "|" + !string.IsNullOrWhiteSpace(message.Text)+"|"+!PlaceholderText);

            if (!string.IsNullOrEmpty(message.Text) && !string.IsNullOrWhiteSpace(message.Text) && !PlaceholderText)
            {

                if (keyboardVisible)
                {
                    ScrollToEnd = false;
                    message.ResignFirstResponder();
                }
                
            }


        }


        // ****************** RETRIVE CONVERSATION'S MESSAGE FROM SERVER ***********************

        public void RetriveMessages()
        {


            //var client = new RestClient("http://location.keepup.pro:3000/");
            //DISTRIBUTION
            var client = new RestClient("http://api.netwintec.com:3000/");

            string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U");

            if (token == "")
            {

                // ERRORE
                return;

            }

            var requestN4U = new RestRequest("api/conversations/get_conversation/" + key + "/" + token, Method.GET);
            requestN4U.AddHeader("content-type", "application/json");


            client.ExecuteAsync(requestN4U,(s,e) => {

                InvokeOnMainThread(() =>
                {

                    IRestResponse response = s;
                    Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        List<Message> listAppoggio = new List<Message>();

                        JsonValue json = JsonValue.Parse(response.Content);
                        int reveal = 0;
                        bool mine = false; 
                        foreach (JsonValue j in json)
                        {

                            var messages = j["messages"];
                            reveal = j["isRevealed"];
                            mine = j["isStartedByMe"];

                            SegnalaKey = j["_key"];

                            string[] IDApp = SegnalaKey.Split(':');
                            if (mine)
                            {
                                SegnalaPersonaId = IDApp[2] + "::" + IDApp[4] + "::" + IDApp[6];
                            }
                            else
                            {
                                SegnalaPersonaId = IDApp[8] + "::" + IDApp[10] + "::" + IDApp[12];
                            }

                            //Console.WriteLine(SegnalaPersonaId + "\n" + IDApp[0] + "::" + IDApp[2] + "::" + IDApp[4] + "::" + IDApp[6] + "::" + IDApp[8] + "::" + IDApp[10] + "::" + IDApp[12]);

                            try
                            {
                                int NotReadMessage = j["unreadMessages"];
                                UIApplication.SharedApplication.ApplicationIconBadgeNumber = NotReadMessage;
                            }
                            catch (Exception ee) { }

                            foreach (JsonValue jj in messages)
                            {



                                string message = jj["message"];
                                bool isMine = jj["isFromMe"];

                                if(isMine)
                                    SegnalaPersonaName = jj["to"];
                                else
                                    SegnalaPersonaName = jj["from"];


                                Message m = new Message(message, isMine);
                                listAppoggio.Add(m);

                            }
                        }

                        //InvokeOnMainThread(() =>
                        //{
                            for (int i = listAppoggio.Count - 1; i >= 0; i--)
                            {

                                list.Add(listAppoggio[i]);
                                //Chat.listMessage.Add(listAppoggio[i]);
                                Chat.element++;
                                //Enumerable.Reverse (list);
                            }



                            if (mine)
                            {
                                if (reveal == 0)
                                {
                                    navBar.SeeReveal();
                                    isRevealed = false;
                                }
                            }
                            chatTable.ReloadData();
                            SopraView.RemoveFromSuperview();
                            chatTable.ScrollToRow(NSIndexPath.FromRowSection(Chat.element-1, 0), UITableViewScrollPosition.Middle, true);
                        //Console.WriteLine (response.Content);
                        //});
                    }
                    else {
                        //if()
                        //InvokeOnMainThread(() =>
                        //{
                            UIViewController lp = HomePage.Instance.storyboard.InstantiateViewController("HomePage");
                            ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                            PresentViewController(lp, true, null);
                        //});
                    }
                });
            });
        }


        // ****************** ADD MESSAGE AFTER NOTIFICATION ***********************

        public void AddNewMessage(string message)
        {

            Message m = new Message(message, false);
            list.Add(m);
            Chat.element++;
            chatTable.ReloadData();

            NSIndexPath bottom = new NSIndexPath();

            chatTable.ScrollToRow(NSIndexPath.FromRowSection(Chat.element-1, 0), UITableViewScrollPosition.Middle, true);
            //NSIndexPath* myIndexPath = [NSIndexPath indexPathForRow: AddYourLastIndex inSection: 0];
            //[self.tbl_presentation selectRowAtIndexPath:myIndexPath animated:YES scrollPosition:UITableViewScrollPositionBottom];

            //var client = new RestClient("http://location.keepup.pro:3000/");
            //DISTRIBUTION
            var client = new RestClient("http://api.netwintec.com:3000/");

            string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenLovieN4U");

            if (token == "")
            {

                // ERRORE
                return;

            }

            var requestN4U = new RestRequest("api/conversations/get_conversation/" + key + "/" + token, Method.GET);
            requestN4U.AddHeader("content-type", "application/json");


            client.ExecuteAsync(requestN4U, (s, e) => {

                InvokeOnMainThread(() =>
                {

                    IRestResponse response = s;
                    Console.WriteLine("RESPONSE:" + response.Content + "|" + response.StatusCode);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        List<Message> listAppoggio = new List<Message>();

                        JsonValue json = JsonValue.Parse(response.Content);
                        foreach (JsonValue j in json)
                        {

                            int NotReadMessage = j["unreadMessages"];
                            UIApplication.SharedApplication.ApplicationIconBadgeNumber = NotReadMessage;

                        }
                    }
                });

            });

        }


        public void CompareNotification()
        {

            CABasicAnimation AlphaIn = new CABasicAnimation();
            AlphaIn.KeyPath = "opacity";
            AlphaIn.BeginTime = 0;
            AlphaIn.Duration = 1.5;
            AlphaIn.From = new NSNumber(0);
            AlphaIn.To = new NSNumber(1);

            CABasicAnimation AlphaOut = new CABasicAnimation();
            AlphaOut.KeyPath = "opacity";
            AlphaOut.BeginTime = 1.5;
            AlphaOut.Duration = 1.5;
            AlphaOut.From = new NSNumber(1);
            AlphaOut.To = new NSNumber(0);

            CAAnimationGroup animationGroup = CAAnimationGroup.CreateAnimation();
            animationGroup.Duration = 3;
            animationGroup.RepeatCount = 1;
            animationGroup.Animations = new CAAnimation[] { AlphaIn, AlphaOut};

            NotificationView.Layer.AddAnimation(animationGroup, null);
        }
            
        //******** REINIZIALIZE PAGE AFTER APP BACKGROUND *********//

        public void ReinizializePage()
        {

            SopraView = new UIView(new CGRect(0, 64, View.Frame.Width, View.Frame.Height - 64));
            SopraView.BackgroundColor = UIColor.FromRGBA(63, 148, 202, 100);

            UIActivityIndicatorView loadspinner = new UIActivityIndicatorView(new CGRect(SopraView.Frame.Width / 2 - 20, SopraView.Frame.Height / 2 - 20, 40, 40));
            loadspinner.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge;
            loadspinner.Color = UIColor.White;
            loadspinner.StartAnimating();

            SopraView.Add(loadspinner);

            list.Clear();
            Chat.element = 0 ;
            chatTable.ReloadData();

            // NON MI TORNA RIGA SOTTO
            Chat.element = 1;

            if (!isFirstMessage)
                RetriveMessages();

            if (!isFirstMessage)
                View.Add(SopraView);

        }

    }

}

